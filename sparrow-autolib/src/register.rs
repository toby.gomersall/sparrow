use quote::{quote, format_ident};
use proc_macro2::{TokenStream as TokenStream2};
use std::collections::HashMap;

use sparrow_map_config::register::{Register, RegisterType};
use crate::to_tokens::ToTokens;

impl ToTokens for Register {

    /// Returns the tokens representing a register, including the associated
    /// bitfields. This is intended to be used as the only set of tokens
    /// within a module, though it need not necessarily be so.
    fn to_tokens(&self, context: &HashMap<String, String>) -> TokenStream2
    {
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");
        let packed_type_ident = format_ident!("{}", packed_type_str);

        let index_ident = format_ident!("U{}", self.index);
        let type_name_ident = format_ident!("{}", "Bitfields".to_string());

        let mut new_context = context.clone();

        let (register_use, register_type, docstring_type) = match self.reg_type {
            RegisterType::ReadOnly => {
                new_context.insert("register_type".to_string(), "RO".to_string());
                (quote!(use sparrow_registers::ReadOnlyRegister;),
                quote!(ReadOnlyRegister<P, #type_name_ident, #index_ident, #packed_type_ident>),
                "Read-only")
            },
            RegisterType::WriteOnly => {
                new_context.insert("register_type".to_string(), "WO".to_string());
                (quote!(use sparrow_registers::WriteOnlyRegister;),
                quote!(WriteOnlyRegister<P, #type_name_ident, #index_ident, #packed_type_ident>),
                "Write-only")
            },
            RegisterType::ReadWrite => {
                new_context.insert("register_type".to_string(), "RW".to_string());
                (quote!(use sparrow_registers::ReadWriteRegister;),
                quote!(ReadWriteRegister<P, #type_name_ident, #index_ident, #packed_type_ident>),
                "Read-write")
            },
        };

        let bitfields_tokens = self.bitfields.to_tokens(&new_context);

        let reg_docstring = match &self.description {
            Some(docstring) => docstring.clone(),
            None => "No description".to_string()
        };

        let module_docstring = format!(
            "**[{}]** {}", docstring_type, reg_docstring);

        quote!(
            #![doc = #module_docstring]

            use typenum::consts::*;
            #register_use

            /// The type that encapsulates all the necessary constraints on
            /// this particular register (specifically, bitfields and index).
            ///
            /// Only the raw peripheral generic type `P` is exposed, which
            /// should be a type that implements the
            /// [`RawPeripheral`](sparrow_raw_peripherals::RawPeripheral) trait.
            /// See the documentation for
            /// [`WriteOnlyRegister`](sparrow_registers::WriteOnlyRegister)
            /// for more information.
            pub type Register<P> = #register_type;

            #bitfields_tokens
        )
    }
}

