use quote::{quote, format_ident};
use proc_macro2::{TokenStream as TokenStream2};
use std::collections::HashMap;

use sparrow_map_config::registers_layout::RegistersLayout;

use crate::to_tokens::ToTokens;

impl ToTokens for RegistersLayout {

    fn to_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");
        let packed_type_ident = format_ident!("{}", packed_type_str);

        let mut layout_tokens = Vec::new();
        let mut register_name_tokens = Vec::new();

        for (register_name, register) in &self.layout {

            let reg_namespace_str = match context.get("namespace") {
                Some(namespace_str) => format!("{}::{}", namespace_str, register_name),
                None => register_name.to_string()
            };

            let mut new_context = context.clone();
            new_context.insert("namespace".to_string(), reg_namespace_str);

            let register_tokens = register.to_tokens(&new_context);
            let register_name_ident = format_ident!("{}", register_name);

            layout_tokens.push(quote!(
                    pub mod #register_name_ident {
                        #register_tokens
                    }
            ));

            register_name_tokens.push(register_name_ident);
        }
        quote!(

            use sparrow_bitpacker::{
                PackedType,
                #packed_type_ident};

            #(#layout_tokens)*
            pub struct RegistersLayout<P> where P: RawPeripheral<#packed_type_ident>
            {
                pub(crate) raw_peripheral: P,
                #(pub #register_name_tokens: #register_name_tokens::Register<P>),*
            }

            impl <P> RegistersLayout<P> where P: RawPeripheral<#packed_type_ident>
            {

                pub fn new(raw_peripheral: P) -> Self {
                    #(let #register_name_tokens =
                      #register_name_tokens::Register::new(&raw_peripheral);)*

                    RegistersLayout::<P> {
                        raw_peripheral, #(#register_name_tokens),* }
                }
            }
        )
    }
}
