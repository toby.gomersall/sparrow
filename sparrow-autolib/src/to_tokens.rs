
use proc_macro2::{TokenStream as TokenStream2};
use std::collections::HashMap;

pub(crate) trait ToTokens {
    fn to_tokens(&self, context: &HashMap<String, String>)
        -> TokenStream2;
}
