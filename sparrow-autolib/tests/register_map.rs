
pub mod registers32 {
    use sparrow_autolib::build_register_lib;
    use sparrow_raw_peripherals::RawPeripheral;
    build_register_lib!("tests/test_registers.yaml", "PackedU32");
}

pub mod registers16 {
    use sparrow_autolib::build_register_lib;
    use sparrow_raw_peripherals::RawPeripheral;
    build_register_lib!("tests/test_registers16.yaml", "PackedU16");
}

#[cfg(test)]
mod register32_tests {
    use super::*;
    use sparrow_raw_peripherals::RawPeripheral;
    use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
    use sparrow_registers::registers::{WriteableRegister, ReadableRegister};
    use sparrow_bitpacker::bitfields::Bitfield;
    use rand::{thread_rng, Rng};

    #[test]
    fn test_register_write() {
        let mut rng = thread_rng();
        let (val_go, packed_go) = if rng.gen() {
            let val_go: bool = rng.gen();
            let packed_go = (val_go as u32) << 0;

            (Some(val_go), packed_go)
        } else {
            (None, registers32::control::Bitfields::default().go.unwrap().pack().unwrap())
        };

        let (val_stop, packed_stop) = if rng.gen() {
            let val_stop: bool = rng.gen();
            let packed_stop = (val_stop as u32) << 1;

            (Some(val_stop), packed_stop)
        } else {
            (None, registers32::control::Bitfields::default().stop.unwrap().pack().unwrap())
        };

        let (val_length, packed_length) = if rng.gen() {
            let val_length: u32 = rng.gen_range(0, 2u32.pow(10));
            let packed_length = (val_length as u32) << 2;

            (Some(val_length), packed_length)
        } else {
            (None, registers32::control::Bitfields::default().length.unwrap().pack().unwrap())
        };

        let bitfield = registers32::control::Bitfields::new(
            val_go, val_stop, val_length).unwrap();

        let mut handler = MemoryHandler::new();
        let p = MockPeripheral::new(&mut handler);

        let mut reg_layout = registers32::RegistersLayout::new(p);

        reg_layout.control.write(bitfield).unwrap();

        // control is the zeroth register
        assert_eq!(
            reg_layout.raw_peripheral.read(0).unwrap(),
            packed_go | packed_stop | packed_length);

    }

    #[test]
    fn test_register_read() {
        let mut rng = thread_rng();

        let (val_isitgood, packed_isitgood) = if rng.gen() {
            let val_isitgood: bool = rng.gen();
            let packed_isitgood = (val_isitgood as u32) << 20;

            (val_isitgood, packed_isitgood)
        } else {
            (false, 0)
        };

        let (val_whatson, packed_whatson) = if rng.gen() {
            let val_whatson: u32 = rng.gen_range(0, 2u32.pow(10));
            let packed_whatson = (val_whatson as u32) << 0;

            (val_whatson, packed_whatson)
        } else {
            (0, 0)
        };

        let mut handler = MemoryHandler::new();
        let p = MockPeripheral::new(&mut handler);

        // Status is register 1, we write the packed values to this
        // location
        p.write(1, packed_isitgood | packed_whatson).unwrap();

        let reg_layout = registers32::RegistersLayout::new(p);
        let read_vals: registers32::status::ExtractedBitfields =
            reg_layout.status.read().unwrap().into();

        assert_eq!(read_vals.isitgood, val_isitgood);
        assert_eq!(read_vals.whatson, val_whatson);
    }

    #[test]
    fn test_register_read_write() {
        let mut rng = thread_rng();

        let (val_size, opt_size) = if rng.gen() {
            let val_size: u32 = rng.gen_range(0, 2u32.pow(4));
            (val_size, Some(val_size))
        } else {
            (registers32::config::Bitfields::default().size.unwrap().val, None)
        };

        let (val_frobinate, opt_frobinate) = if rng.gen() {
            let val_frobinate: bool = rng.gen();
            (val_frobinate, Some(val_frobinate))
        } else {
            (registers32::config::Bitfields::default().frobinate.unwrap().val, None)
        };

        let (val_power, opt_power) = if rng.gen() {
            let val_power: u32 = rng.gen_range(0, 2u32.pow(4));
            (val_power, Some(val_power))
        } else {
            (registers32::config::Bitfields::default().power.unwrap().val, None)
        };

        let bitfield = registers32::config::Bitfields::new(
            opt_size, opt_power, opt_frobinate).unwrap();

        let mut handler = MemoryHandler::new();
        let p = MockPeripheral::new(&mut handler);

        let mut reg_layout = registers32::RegistersLayout::new(p);

        reg_layout.config.write(bitfield).unwrap();

        let read_vals: registers32::config::ExtractedBitfields =
            reg_layout.config.read().unwrap().into();

        assert_eq!(read_vals.size, val_size);
        assert_eq!(read_vals.power, val_power);
        assert_eq!(read_vals.frobinate, val_frobinate);
    }
}

#[cfg(test)]
mod bitfield32_tests {
    use super::*;
    use rand::{thread_rng, Rng};
    use sparrow_bitpacker::Bitfield;
    use crypto::{digest::Digest, sha1::Sha1};

    #[test]
    /// It should be possible to output a packed 32-bit word from a Bitfields
    /// object. This should be based on the boolean-or of each of the packed
    /// sub-elements of the bitfield.
    /// Default values should be read from the `default` field, which itself
    /// defaults to the type equivalent of zero (ie `0u32`, `false` etc).
    fn test_bitfield_packing() {

        let mut rng = thread_rng();

        let val_go: bool = rng.gen();
        let packed_go = (val_go as u32) << 0;
        let bitfield = registers32::control::Bitfields::new(
            Some(val_go), None, None).unwrap();
        assert_eq!(bitfield.pack().unwrap(), packed_go);

        let val_stop: bool = rng.gen();
        let packed_stop = (val_stop as u32) << 1;
        let bitfield = registers32::control::Bitfields::new(
            None, Some(val_stop), None).unwrap();
        assert_eq!(bitfield.pack().unwrap(), packed_stop);

        let val_length: u32 = rng.gen_range(0, 2u32.pow(10));
        let packed_length = (val_length as u32) << 2;
        let bitfield = registers32::control::Bitfields::new(
            None, None, Some(val_length)).unwrap();

        assert_eq!(bitfield.pack().unwrap(), packed_length);

        let bitfield = registers32::control::Bitfields::new(
            Some(val_go), None, Some(val_length)).unwrap();

        assert_eq!(bitfield.pack().unwrap(), packed_go | packed_length);

        let bitfield = registers32::control::Bitfields::new(
            Some(val_go), Some(val_stop), Some(val_length)).unwrap();

        assert_eq!(bitfield.pack().unwrap(), packed_go | packed_stop | packed_length);

    }

    #[test]
    /// During packing, default values should be read from the `default` field,
    /// which itself defaults to the type equivalent of zero (ie `0u32`,
    /// `false` etc).
    fn test_bitfield_packing_with_default() {

        // meta-test first to confirm the yaml is as expected
        use sparrow_map_config::{registers_layout_from_file, bitfields::BitfieldTypes};
        let reg_layout = registers_layout_from_file("tests/test_registers.yaml").unwrap();
        let packed_size =
            match &reg_layout.layout["config"].bitfields.fields["size"] {
                BitfieldTypes::Uint(def) => {
                    assert_eq!(def.default, None);
                    0 << def.offset},
                _ => panic!(),
            };
        let packed_power =
            match &reg_layout.layout["config"].bitfields.fields["power"] {
                BitfieldTypes::Uint(def) => {
                    assert_eq!(def.default, Some(15));
                    15 << def.offset},
                _ => panic!(),
            };
        let packed_frobinate =
            match &reg_layout.layout["config"].bitfields.fields["frobinate"] {
                BitfieldTypes::Bool(def) => {
                    assert_eq!(def.default, Some(true));
                    1 << def.offset},
                _ => panic!(),
            };

        let bitfield = registers32::config::Bitfields::new(
            None, None, None).unwrap();

        assert_eq!(
            bitfield.pack().unwrap(),
            packed_size | packed_power | packed_frobinate);

    }

    #[test]
    /// It should be possible to construct a bitfield from the `unpack`
    /// method which should populate the fields properly based on the
    /// type.
    fn test_bitfield_unpacking() {
        let mut rng = thread_rng();

        let go: bool = rng.gen();
        let stop: bool = rng.gen();
        let length: u32 = rng.gen_range(0, 2u32.pow(10));
        let bitfield = registers32::control::Bitfields::new(
            Some(go), Some(stop), Some(length)).unwrap();

        let packed_val = bitfield.pack().unwrap();

        assert_eq!(registers32::control::Bitfields::unpack(packed_val).unwrap(), 
                   bitfield);
    }

    #[test]
    /// Calling the [`update`] method on a [`Bitfield`] should update the 
    /// fields with those in the argument.
    fn test_bitfield_update() {

        let mut bf = registers32::control::Bitfields::new(
            Some(true), None, Some(15)).unwrap();
        let updating_bf = registers32::control::Bitfields::new(
            None, Some(true), Some(512)).unwrap();

        bf.update(&updating_bf);

        assert_eq!(bf, registers32::control::Bitfields::new(
                Some(true), Some(true), Some(512)).unwrap());

        let mut bf = registers32::control::Bitfields::new(
            None, None, None).unwrap();
        let updating_bf = registers32::control::Bitfields::new(
            None, None, None).unwrap();

        bf.update(&updating_bf);

        assert_eq!(bf, registers32::control::Bitfields::new(
                None, None, None).unwrap());

        let mut bf = registers32::control::Bitfields::new(
            Some(false), None, Some(15)).unwrap();
        let updating_bf = registers32::control::Bitfields::new(
            Some(true), None, Some(512)).unwrap();

        bf.update(&updating_bf);

        assert_eq!(bf, registers32::control::Bitfields::new(
                Some(true), None, Some(512)).unwrap());
    }

    #[test]
    /// The [`Into`] trait should be available on a [`Bitfield`] object in 
    /// order to extract the internal values into an [`ExtractedBitfield`] 
    /// object for further use.
    ///
    /// Empty (`None`) fields should map to their equivalent "zero" value 
    /// (i.e. 0 or false).
    fn test_bitfield_into() {

        let bf = registers32::control::Bitfields::new(
            None, Some(false), Some(12)).unwrap();
        let ex_bf: registers32::control::ExtractedBitfields = bf.into();

        assert_eq!(ex_bf.go, false);
        assert_eq!(ex_bf.stop, false);
        assert_eq!(ex_bf.length, 12);
    }

    #[test]
    /// A uint_sha1_hash bitfield should generate a verify_word method that
    /// when called will check against the relevant portion of the SHA1 hash
    /// of the YAML file contents. The relevant portion should be determined
    /// by the digest-digits and digest-offset keys of the YAML file which
    /// refer to the number of hex digits and the offset from the left in hex 
    /// digits respectively of the hash to be verified.
    fn test_verify_uint_sha1_hash_bitfield() {

        // Firstly check an invalid value fails:
        let bf = registers32::hex_digest::Bitfields::new(Some(0)).unwrap();
        assert!(!bf.verify_word());

        let yaml_string = std::fs::read_to_string(
            "tests/test_registers.yaml").unwrap();

        let mut hasher = Sha1::new();
        hasher.input_str(&yaml_string);
        let yaml_hex_digest = hasher.result_str();

        let mut digits = Vec::new();
        for digit in yaml_hex_digest.chars() {
            digits.push(digit.to_digit(16).unwrap());
        }

        const DIGEST_DIGITS: usize = 8usize;
        const DIGEST_OFFSET: usize = 0usize;
        const DIGIT_SIZE: u32 = 4;

        let mut expected_word = 0;
        for n in DIGEST_OFFSET..(DIGEST_OFFSET + DIGEST_DIGITS) {
            expected_word = (expected_word << DIGIT_SIZE) | digits[n];
        }

        let bf = registers32::hex_digest::Bitfields::new(Some(expected_word)).unwrap();
        assert!(bf.verify_word());


    }

    mod fields {
        use super::*;

        #[test]
        /// There should be a simple API to return a Bitfield with just the
        /// go field set.
        fn test_go() {
            assert_eq!(
                registers32::control::fields::go(true),
                registers32::control::Bitfields::new(Some(true), None, None));

            assert_eq!(
                registers32::control::fields::go(false),
                registers32::control::Bitfields::new(Some(false), None, None));
        }

        #[test]
        /// There should be a simple API to return a Bitfield with just the
        /// stop field set.
        fn test_stop() {
            assert_eq!(
                registers32::control::fields::stop(true),
                registers32::control::Bitfields::new(None, Some(true), None));

            assert_eq!(
                registers32::control::fields::stop(false),
                registers32::control::Bitfields::new(None, Some(false), None));
        }

        #[test]
        /// There should be a simple API to return a Bitfield with just the
        /// go field set.
        fn test_length() {
            assert_eq!(
                registers32::control::fields::length(12),
                registers32::control::Bitfields::new(None, None, Some(12)));
        }

        #[test]
        /// Errors from the fields constructors should be propagated as expected
        fn test_length_error() {
            use sparrow_bitpacker::BitfieldError;

            const MAX_VAL: u32 = !0u32 >> (32 - 10);
            let test_val = MAX_VAL + 10;

            let res = registers32::control::fields::length(test_val);
            if let Err(error) = res {
                if let BitfieldError::OverRange { description: _ } = error {
                    assert!(true);
                } else {
                    panic!("The error should be a BitfieldError::OverRange");
                }
            } else {
                panic!("The bitfield should error");
            }

        }

    }
}

#[cfg(test)]
mod register16_tests {
    use super::*;
    use sparrow_raw_peripherals::RawPeripheral;
    use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
    use sparrow_registers::registers::{WriteableRegister, ReadableRegister};
    use sparrow_bitpacker::bitfields::Bitfield;
    use sparrow_bitpacker::PackedU16;
    use rand::{thread_rng, Rng};

    #[test]
    fn test_register_write() {
        let mut rng = thread_rng();

        let (val_go, packed_go) = if rng.gen() {
            let val_go: bool = rng.gen();
            let packed_go = (val_go as u16) << 0;

            (Some(val_go), packed_go)
        } else {
            (None, registers16::control::Bitfields::default().go.unwrap().pack().unwrap())
        };

        let (val_stop, packed_stop) = if rng.gen() {
            let val_stop: bool = rng.gen();
            let packed_stop = (val_stop as u16) << 1;

            (Some(val_stop), packed_stop)
        } else {
            (None, registers16::control::Bitfields::default().stop.unwrap().pack().unwrap())
        };

        let (val_length, packed_length) = if rng.gen() {
            let val_length: u16 = rng.gen_range(0, 2u16.pow(10));
            let packed_length = (val_length as u16) << 2;

            (Some(val_length), packed_length)
        } else {
            (None, registers16::control::Bitfields::default().length.unwrap().pack().unwrap())
        };

        let bitfield = registers16::control::Bitfields::new(
            val_go, val_stop, val_length).unwrap();

        let mut handler = MemoryHandler::<PackedU16>::new();
        let p = MockPeripheral::new(&mut handler);

        let mut reg_layout = registers16::RegistersLayout::new(p);

        reg_layout.control.write(bitfield).unwrap();

        // control is the zeroth register
        assert_eq!(
            reg_layout.raw_peripheral.read(0).unwrap(),
            packed_go | packed_stop | packed_length);

    }

    #[test]
    fn test_register_read() {
        let mut rng = thread_rng();

        let (val_isitgood, packed_isitgood) = if rng.gen() {
            let val_isitgood: bool = rng.gen();
            let packed_isitgood = (val_isitgood as u16) << 14;

            (val_isitgood, packed_isitgood)
        } else {
            (false, 0)
        };

        let (val_whatson, packed_whatson) = if rng.gen() {
            let val_whatson: u16 = rng.gen_range(0, 2u16.pow(10));
            let packed_whatson = (val_whatson as u16) << 0;

            (val_whatson, packed_whatson)
        } else {
            (0, 0)
        };

        let mut handler = MemoryHandler::<PackedU16>::new();
        let p = MockPeripheral::new(&mut handler);

        // Status is register 1, we write the packed values to this
        // location
        p.write(1, packed_isitgood | packed_whatson).unwrap();

        let reg_layout = registers16::RegistersLayout::new(p);
        let read_vals: registers16::status::ExtractedBitfields =
            reg_layout.status.read().unwrap().into();

        assert_eq!(read_vals.isitgood, val_isitgood);
        assert_eq!(read_vals.whatson, val_whatson);
    }

    #[test]
    fn test_register_read_write() {
        let mut rng = thread_rng();

        let (val_size, opt_size) = if rng.gen() {
            let val_size: u16 = rng.gen_range(0, 2u16.pow(4));
            (val_size, Some(val_size))
        } else {
            (registers16::config::Bitfields::default().size.unwrap().val, None)
        };

        let (val_frobinate, opt_frobinate) = if rng.gen() {
            let val_frobinate: bool = rng.gen();
            (val_frobinate, Some(val_frobinate))
        } else {
            (registers16::config::Bitfields::default().frobinate.unwrap().val, None)
        };

        let (val_power, opt_power) = if rng.gen() {
            let val_power: u16 = rng.gen_range(0, 2u16.pow(4));
            (val_power, Some(val_power))
        } else {
            (registers16::config::Bitfields::default().power.unwrap().val, None)
        };

        let bitfield = registers16::config::Bitfields::new(
            opt_size, opt_power, opt_frobinate).unwrap();

        let mut handler = MemoryHandler::<PackedU16>::new();
        let p = MockPeripheral::new(&mut handler);

        let mut reg_layout = registers16::RegistersLayout::new(p);

        reg_layout.config.write(bitfield).unwrap();

        let read_vals: registers16::config::ExtractedBitfields =
            reg_layout.config.read().unwrap().into();

        assert_eq!(read_vals.size, val_size);
        assert_eq!(read_vals.power, val_power);
        assert_eq!(read_vals.frobinate, val_frobinate);
    }
}

#[cfg(test)]
mod bitfield16_tests {
    use super::*;
    use rand::{thread_rng, Rng};
    use sparrow_bitpacker::Bitfield;
    use crypto::{digest::Digest, sha1::Sha1};

    #[test]
    /// It should be possible to output a packed 32-bit word from a Bitfields
    /// object. This should be based on the boolean-or of each of the packed
    /// sub-elements of the bitfield.
    /// Default values should be read from the `default` field, which itself
    /// defaults to the type equivalent of zero (ie `0u32`, `false` etc).
    fn test_bitfield_packing() {

        let mut rng = thread_rng();

        let val_go: bool = rng.gen();
        let packed_go = (val_go as u16) << 0;
        let bitfield = registers16::control::Bitfields::new(
            Some(val_go), None, None).unwrap();
        assert_eq!(bitfield.pack().unwrap(), packed_go);

        let val_stop: bool = rng.gen();
        let packed_stop = (val_stop as u16) << 1;
        let bitfield = registers16::control::Bitfields::new(
            None, Some(val_stop), None).unwrap();
        assert_eq!(bitfield.pack().unwrap(), packed_stop);

        let val_length: u16 = rng.gen_range(0, 2u16.pow(10));
        let packed_length = (val_length as u16) << 2;
        let bitfield = registers16::control::Bitfields::new(
            None, None, Some(val_length)).unwrap();

        assert_eq!(bitfield.pack().unwrap(), packed_length);

        let bitfield = registers16::control::Bitfields::new(
            Some(val_go), None, Some(val_length)).unwrap();

        assert_eq!(bitfield.pack().unwrap(), packed_go | packed_length);

        let bitfield = registers16::control::Bitfields::new(
            Some(val_go), Some(val_stop), Some(val_length)).unwrap();

        assert_eq!(bitfield.pack().unwrap(), packed_go | packed_stop | packed_length);

    }

    #[test]
    /// During packing, default values should be read from the `default` field,
    /// which itself defaults to the type equivalent of zero (ie `0u16`,
    /// `false` etc).
    fn test_bitfield_packing_with_default() {

        // meta-test first to confirm the yaml is as expected
        use sparrow_map_config::{registers_layout_from_file, bitfields::BitfieldTypes};
        let reg_layout = registers_layout_from_file("tests/test_registers.yaml").unwrap();
        let packed_size =
            match &reg_layout.layout["config"].bitfields.fields["size"] {
                BitfieldTypes::Uint(def) => {
                    assert_eq!(def.default, None);
                    0 << def.offset},
                _ => panic!(),
            };
        let packed_power =
            match &reg_layout.layout["config"].bitfields.fields["power"] {
                BitfieldTypes::Uint(def) => {
                    assert_eq!(def.default, Some(15));
                    15 << def.offset},
                _ => panic!(),
            };
        let packed_frobinate =
            match &reg_layout.layout["config"].bitfields.fields["frobinate"] {
                BitfieldTypes::Bool(def) => {
                    assert_eq!(def.default, Some(true));
                    1 << def.offset},
                _ => panic!(),
            };

        let bitfield = registers16::config::Bitfields::new(
            None, None, None).unwrap();

        assert_eq!(
            bitfield.pack().unwrap(),
            packed_size | packed_power | packed_frobinate);

    }

    #[test]
    /// It should be possible to construct a bitfield from the `unpack`
    /// method which should populate the fields properly based on the
    /// type.
    fn test_bitfield_unpacking() {
        let mut rng = thread_rng();

        let go: bool = rng.gen();
        let stop: bool = rng.gen();
        let length: u16 = rng.gen_range(0, 2u16.pow(10));
        let bitfield = registers16::control::Bitfields::new(
            Some(go), Some(stop), Some(length)).unwrap();

        let packed_val = bitfield.pack().unwrap();

        assert_eq!(registers16::control::Bitfields::unpack(packed_val).unwrap(), 
                   bitfield);
    }

    #[test]
    /// Calling the [`update`] method on a [`Bitfield`] should update the 
    /// fields with those in the argument.
    fn test_bitfield_update() {

        let mut bf = registers16::control::Bitfields::new(
            Some(true), None, Some(15)).unwrap();
        let updating_bf = registers16::control::Bitfields::new(
            None, Some(true), Some(512)).unwrap();

        bf.update(&updating_bf);

        assert_eq!(bf, registers16::control::Bitfields::new(
                Some(true), Some(true), Some(512)).unwrap());

        let mut bf = registers16::control::Bitfields::new(
            None, None, None).unwrap();
        let updating_bf = registers16::control::Bitfields::new(
            None, None, None).unwrap();

        bf.update(&updating_bf);

        assert_eq!(bf, registers16::control::Bitfields::new(
                None, None, None).unwrap());

        let mut bf = registers16::control::Bitfields::new(
            Some(false), None, Some(15)).unwrap();
        let updating_bf = registers16::control::Bitfields::new(
            Some(true), None, Some(512)).unwrap();

        bf.update(&updating_bf);

        assert_eq!(bf, registers16::control::Bitfields::new(
                Some(true), None, Some(512)).unwrap());
    }

    #[test]
    /// The [`Into`] trait should be available on a [`Bitfield`] object in 
    /// order to extract the internal values into an [`ExtractedBitfield`] 
    /// object for further use.
    ///
    /// Empty (`None`) fields should map to their equivalent "zero" value 
    /// (i.e. 0 or false).
    fn test_bitfield_into() {

        let bf = registers16::control::Bitfields::new(
            None, Some(false), Some(12)).unwrap();
        let ex_bf: registers16::control::ExtractedBitfields = bf.into();

        assert_eq!(ex_bf.go, false);
        assert_eq!(ex_bf.stop, false);
        assert_eq!(ex_bf.length, 12);
    }

    #[test]
    /// A uint_sha1_hash bitfield should generate a verify_word method that
    /// when called will check against the relevant portion of the SHA1 hash
    /// of the YAML file contents. The relevant portion should be determined
    /// by the digest-digits and digest-offset keys of the YAML file which
    /// refer to the number of hex digits and the offset from the left in hex 
    /// digits respectively of the hash to be verified.
    fn test_verify_uint_sha1_hash_bitfield() {

        // Firstly check an invalid value fails:
        let bf = registers16::hex_digest::Bitfields::new(Some(0)).unwrap();
        assert!(!bf.verify_word());

        let yaml_string = std::fs::read_to_string(
            "tests/test_registers16.yaml").unwrap();

        let mut hasher = Sha1::new();
        hasher.input_str(&yaml_string);
        let yaml_hex_digest = hasher.result_str();

        let mut digits = Vec::<u16>::new();
        for digit in yaml_hex_digest.chars() {
            digits.push(digit.to_digit(16).unwrap() as u16);
        }

        const DIGEST_DIGITS: usize = 4usize;
        const DIGEST_OFFSET: usize = 0usize;
        const DIGIT_SIZE: u16 = 4;

        let mut expected_word: u16 = 0;
        for n in DIGEST_OFFSET..(DIGEST_OFFSET + DIGEST_DIGITS) {
            expected_word = (expected_word << DIGIT_SIZE) | digits[n];
        }

        // Ordinarily, the value itself will be read from a register, but we're 
        // testing the bitfields here so we can't do that so we write it
        // ourselves.
        let bf = registers16::hex_digest::Bitfields::new(Some(expected_word)).unwrap();
        assert!(bf.verify_word());
    }

    mod fields {
        use super::*;

        #[test]
        /// There should be a simple API to return a Bitfield with just the
        /// go field set.
        fn test_go() {
            assert_eq!(
                registers16::control::fields::go(true),
                registers16::control::Bitfields::new(Some(true), None, None));

            assert_eq!(
                registers16::control::fields::go(false),
                registers16::control::Bitfields::new(Some(false), None, None));
        }

        #[test]
        /// There should be a simple API to return a Bitfield with just the
        /// stop field set.
        fn test_stop() {
            assert_eq!(
                registers16::control::fields::stop(true),
                registers16::control::Bitfields::new(None, Some(true), None));

            assert_eq!(
                registers16::control::fields::stop(false),
                registers16::control::Bitfields::new(None, Some(false), None));
        }

        #[test]
        /// There should be a simple API to return a Bitfield with just the
        /// go field set.
        fn test_length() {
            assert_eq!(
                registers16::control::fields::length(12),
                registers16::control::Bitfields::new(None, None, Some(12)));
        }

        #[test]
        /// Errors from the fields constructors should be propagated as expected
        fn test_length_error() {
            use sparrow_bitpacker::BitfieldError;

            const MAX_VAL: u16 = !0u16 >> (16 - 10);
            let test_val = MAX_VAL + 10;

            let res = registers16::control::fields::length(test_val);
            if let Err(error) = res {
                if let BitfieldError::OverRange { description: _ } = error {
                    assert!(true);
                } else {
                    panic!("The error should be a BitfieldError::OverRange");
                }
            } else {
                panic!("The bitfield should error");
            }
        }

    }
}
