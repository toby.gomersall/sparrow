use std::mem::size_of;
use snafu::Snafu;
use std::ops::Add;
use typenum::consts::*;
use typenum::{Unsigned, Less, Cmp, Sum, NonZero};
use std::marker::PhantomData;
use std::default::Default;
use crate::{PackedType, PackedU32, TypeLength};
use num::{NumCast, Zero, One};

const _BYTE_BITS: usize = 8;

const fn _word_size<T>() -> usize {
    size_of::<T>() * _BYTE_BITS
}

#[derive(Debug, PartialEq, Snafu)]
pub enum BitfieldError {
    #[snafu(display("Cannot set bitfield with an out of range value: {}", description))]
    OverRange { description: String },
    #[snafu(display("The unpacked value is invalid: {}", description))]
    InvalidUnpackedValue { description: String },
}

pub type BitfieldResult<T, E = BitfieldError> = std::result::Result<T, E>;

/// Represents a bitfield object that can be packed or unpacked into or from
/// an integer type.
///
/// Implementations might handle multiple discrete bitfields as part of one
/// Bitfield object.
pub trait Bitfield<T = PackedU32>: Sized
where T: PackedType + PartialEq,
{
    /// Packs the bitfield value properly aligned into a u32 and returns
    /// the result.
    fn pack(&self) -> Result<T::Type, BitfieldError>;

    /// Takes a packed word and tries to unpack it according to its layout 
    /// which is specified by the concrete implementation.
    ///
    /// Bits that lie outside of the bitfield are ignored.
    ///
    /// Errors occur if the bitfield specification does not support the value
    /// contained in the respective bits of `raw_val` (e.g. it is out of 
    /// range).
    fn unpack(raw_val: T::Type) -> BitfieldResult<Self>;
}

/// Provides the length of the bitfield through the `Length` associated type
/// (as a `typenum`).
pub trait BitfieldLength: Sized
{
    type Length: Unsigned;
}

// Ideally the bitfield types would be implemented with a const generic for 
// offset and length, but these aren't yet supported.
// See RFC 2000: https://github.com/rust-lang/rust/issues/44580
//
// Instead we use typenum, which does much the same (and possibly more)
//
/// A `BoolBitfield` represents a single bit boolean at any position in a
/// 32-bit word.
/// 
/// Creation of the bitfield should be done through the [`new`](#method.new)
/// method.
///
/// To create a boolean bitfield `true` in the 24th bit from the right (
/// indexing from 0):
/// ```
/// # use sparrow_bitpacker::BoolBitfield;
/// use typenum::consts::*;
/// let my_bitfield = BoolBitfield::<U24>::new(true).unwrap();
/// ```
#[derive(Debug, PartialEq, Copy, Clone)]
pub struct BoolBitfield <Offset, T = PackedU32>
{
    pub val: bool,
    marker: PhantomData<Offset>,
    marker_t: PhantomData<T>,
}

impl <Offset, T> BitfieldLength for BoolBitfield <Offset, T>
{
    type Length = U1;
}

impl <Offset, T> BoolBitfield <Offset, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      T: PackedType + PartialEq,
{
    /// Constructor for a `BoolBitfield`, protecting against invalid
    /// arguments.
    ///
    /// The offset of the boolean is set using type based constants from
    /// the [`typenum`][typenum] crate.
    ///
    /// ```
    /// # use sparrow_bitpacker::BoolBitfield;
    /// use typenum::consts::*;
    ///
    /// type Offset = U10;
    /// let my_bitfield = BoolBitfield::<Offset>::new(true).unwrap();
    /// ```
    /// The [typenum constant][typenum::consts] should be `Unsigned` and 
    /// strictly less than 32. i.e. U10 is valid, U32 and P10 are invalid.
    /// The validity is checked at compile time and invalid offsets will cause
    /// the compile to fail, hopefully with a useful error message as to why 
    /// the constant is invalid.
    ///
    /// The following will not compile:
    /// ```compile_fail
    /// # use sparrow_bitpacker::BoolBitfield;
    /// use typenum::consts::*;
    /// let my_bitfield = BoolBitfield::<U32>::new(true).unwrap();
    /// ```
    /// ```compile_fail
    /// # use sparrow_bitpacker::BoolBitfield;
    /// use typenum::consts::*;
    /// let my_bitfield = BoolBitfield::<P10>::new(true).unwrap();
    /// ```
    pub fn new(val: bool) -> BitfieldResult<BoolBitfield<Offset, T>> {
        Ok(BoolBitfield::<Offset, T> {
            val, marker: PhantomData, marker_t: PhantomData})
    }

    /// Returns the inner value contained by this bitfield.
    pub fn into_inner(self) -> bool {
        self.into()
    }

}

impl <Offset, T> Bitfield <T> for BoolBitfield <Offset, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      T: PackedType + PartialEq,
{

    fn pack(&self) -> BitfieldResult<T::Type> {
        if self.val {
            let output = T::Type::one();
            Ok(output << NumCast::from(Offset::USIZE).unwrap())
        } else {
            Ok(T::Type::zero())
        }
    }

    fn unpack(raw_val: T::Type) -> BitfieldResult<BoolBitfield<Offset, T>> {

        let one = T::Type::one();
        let mask: T::Type = one << NumCast::from(Offset::USIZE).unwrap();
        Self::new((raw_val & mask) != T::Type::zero())
    }
}

impl <Offset, T>  Into<bool> for BoolBitfield <Offset, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      T: PackedType + PartialEq,
{
    fn into(self) -> bool {
        self.val
    }
}

impl <Offset, T>  Default for BoolBitfield <Offset, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      T: PackedType + PartialEq,
{
    fn default() -> Self {
        Self::new(false).expect("Error getting the default. This is a bug.")
    }
}


/// A `UintBitfield` represents an unsigned integer bitfield of arbitrary length 
/// up to [T::PackedLength](PackedType::PackedLength) at any position in a T::PackedLength-bit word, 
/// subject to the bitfield fitting within the type [T::Type](PackedType::Type) 
/// (which defaults to u32 through the default `T` of [PackedU32]).
///
/// The protection on the bitfield correctness is enforced through compile
/// time type constraints and all creations being done through the
/// [`new`](#method.new) method. Extensive use is made of [typenum] 
/// to enforce the compile time trait bounds.
/// ```
/// use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VAL: u32 = 13;
/// type Offset = U22;
/// type Length = U5;
/// let my_bitfield = UintBitfield::<Offset, Length>::new(VAL).unwrap();
/// ```
/// In this case, the default `T` of [PackedU32] is used. So,
/// if the given offset and the length result in a bitfield that will not
/// fit inside a u32, the compile will fail. Specifically, the sum of the
/// offset and the length must be less than or equal to 32.
///
/// So the following will fail:
/// ```compile_fail
/// # use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VALUE: u32 = 15;
/// type Offset = U28;
/// type Length = U5; // 28 + 5 = 33 (> 32)
/// let res = UintBitfield::<Offset, Length>::new(VALUE);
/// ```
/// But the following is ok:
/// ```
/// # use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VALUE: u32 = 15;
/// type Offset = U28;
/// type Length = U4; // 28 + 4 = 32 (ok!)
/// let res = UintBitfield::<Offset, Length>::new(VALUE);
/// ```
/// Zero length bitfields are also disallowed and cause a compile error.
/// ```compile_fail
/// # use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VALUE: u32 = 0;
/// type Offset = U20;
/// type Length = U0;
/// let res = UintBitfield::<Offset, Length>::new(VALUE);
/// ```
/// Changing the packed type is easy, and the main uint types are defined as 
/// simple full-width packing types:
/// ```
/// use typenum::consts::U6;
/// use sparrow_bitpacker::{UintBitfield, PackedU16};
///
/// const VALUE: u16 = 15;
/// type Offset = U6;
/// type Length = U6;
/// let u16_bitfield = UintBitfield::<Offset, Length, PackedU16>::new(VALUE).unwrap();
/// ```
/// It is also possible to define a different type for the packing by implementing
/// the [PackedType] trait. For example in the case in which we want to use and 
/// restrict access to only 9 bits of a 16-bit uint:
/// ```
/// // TypeLength is the extension traits to provide the uint types
/// // with length information
/// use typenum::consts::*;
/// use sparrow_bitpacker::{UintBitfield, TypeLength, PackedType};
///
/// #[derive(PartialEq, Copy, Clone)]
/// struct PackedU9 {}
/// 
/// impl PackedType for PackedU9 {
///     type Type = u16;
///     type PackedLength = U9;
/// }
///
/// const VALUE: u16 = 15;
/// type Offset = U3;
/// type Length = U6;
/// let u9_bitfield = UintBitfield::<Offset, Length, PackedU9>::new(VALUE).unwrap();
/// ```
/// Using such a [PackedType] would result in a compile failure if the bitfield
/// does not fit inside [PackedType::PackedLength]:
/// ```compile_fail
/// # use typenum::consts::*;
/// # use sparrow_bitpacker::{UintBitfield, TypeLength, PackedType};
/// # 
/// # #[derive(PartialEq, Copy, Clone)]
/// # struct PackedU9 {}
/// # impl PackedType for PackedU9 {
/// #     type Type = u16;
/// #     type PackedLength = U9;
/// # }
/// #
/// const VALUE: u16 = 15;
/// type Offset = U6;
/// type Length = U4; // <- The length + offset > 9 so fails (but note, is less than 16)
/// let u9_bitfield = UintBitfield::<Offset, Length, PackedU9>::new(VALUE).unwrap();
/// ```
#[derive(Debug, PartialEq, Copy, Clone)]
pub struct UintBitfield <Offset, Length, T = PackedU32>
where T: PackedType + PartialEq,
{
    pub val: T::Type,
    marker_offset: PhantomData<Offset>,
    marker_length: PhantomData<Length>,
    marker_t: PhantomData<T>,
}

impl <Offset, Length, T> BitfieldLength for UintBitfield <Offset, Length, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero,
      <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>, // That is <= 32 
      T: PackedType + PartialEq,

{
    type Length = Length;
}

impl <Offset, Length, T> UintBitfield <Offset, Length, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero,
      <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>, // That is <= 32 
      T: PackedType + PartialEq,
// The above traits are rather subtle.
// The Offset must be less than T::PackedLength
// The Length must be less than T::PackedLength + 1
// The Length + Offset must be less than T::PackedLength + 1
// Length requires the Add trait so it can be represented as such. We omit
// to use the Sum<> notation as Add makes it explicit why we include the Add
// trait with the Length bounds.
{
    /// Constructor for a `UintBitfield`, protecting against invalid
    /// arguments.
    ///
    /// The offset and length of the unsigned integer is set using type based
    /// constants from the [`typenum`][typenum] crate.
    /// ```
    /// # use sparrow_bitpacker::UintBitfield;
    /// use typenum::consts::*;
    /// const VALUE: u32 = 15;
    /// type Offset = U10;
    /// type Length = U6;
    /// let my_bitfield = UintBitfield::<Offset, Length>::new(VALUE).unwrap();
    /// ```
    ///
    /// If the value cannot fit inside the bitfield given the length, a
    /// [`Bitfield::OverRange`](enum.BitfieldError.html#variant.OverRange)
    /// will be raised.
    /// ```
    /// # use sparrow_bitpacker::{UintBitfield, BitfieldError};
    /// use typenum::consts::*;
    /// use typenum::Unsigned;
    /// const VALUE: u32 = 95;
    /// type Offset = U20;
    /// type Length = U6;
    /// let max_val = 2_u32.pow(Length::to_u32()) - 1;
    /// let res = UintBitfield::<Offset, Length>::new(VALUE);
    ///
    /// // We don't need to worry about the exact error message...
    /// if let Err(error) = res {
    ///     if let BitfieldError::OverRange { description: _ } = error {
    ///         assert!(true);
    ///     } else {
    ///         panic!("The error should be a BitfieldError::OverRange");
    ///     }
    /// } else {
    ///     panic!("The bitfield should error");
    /// }
    /// ```
    pub fn new(val: T::Type) -> BitfieldResult<UintBitfield<Offset, Length, T>> {

        let zero = T::Type::zero();
        let ones = !zero;

        // To get the max value, we need to zero the upper bits between the 
        // packed length and the full length using a left shift, then shift 
        // the whole thing down using a right shift so that we only have bits 
        // corresponding to a full bitfield.
        let shift_left = NumCast::from(
            <T::Type as TypeLength>::Length::USIZE - 
            T::PackedLength::USIZE).expect(
                "This should never fail and so is a bug.");

        let shift_right = shift_left + NumCast::from(
            T::PackedLength::USIZE - Length::USIZE).expect(
                "This should never fail and so is a bug.");

        let max_val = (ones << shift_left) >> shift_right;

        if val > max_val {
            let description = format!(
                "{:?} is greater than the maximum allowed value ({:?})", val, max_val);
            Err(BitfieldError::OverRange {description})
        } else {
            Ok(UintBitfield::<Offset, Length, T> {
                val,
                marker_offset: PhantomData,
                marker_length: PhantomData,
                marker_t: PhantomData})
        }
    }

    pub fn into_inner(self) -> T::Type {
        self.val
    }
}

impl <Offset, Length, T> Bitfield <T> for UintBitfield <Offset, Length, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero,
      <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>, // That is <= 32
      T: PackedType + PartialEq,
{

    fn pack(&self) -> BitfieldResult<T::Type> {
        Ok(self.val << NumCast::from(Offset::USIZE).unwrap())
    }

    fn unpack(raw_val: T::Type) -> BitfieldResult<UintBitfield<Offset, Length, T>> {

        let zero = T::Type::zero();
        let mask: T::Type = (!zero >>
            NumCast::from(<T::Type as TypeLength>::Length::USIZE
                          - Length::USIZE).unwrap())
            << NumCast::from(Offset::USIZE).unwrap();

        let shifted_val = mask & raw_val;

        UintBitfield::<Offset, Length, T>::new(
            shifted_val >> NumCast::from(Offset::USIZE).unwrap())
    }
}

// This doesn't work because it conflicts with the blanket Into derived from 
// From. Instead we use into_inner defined on this struct alone.
//impl <Offset, Length, T> Into<T::Type> for UintBitfield <Offset, Length, T>
//where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
//      Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero,
//      <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>,
//      T: PackedType + PartialEq,
//{
//    fn into(self) -> T::Type {
//        self.val
//    }
//
//}

impl <Offset, Length, T> Default for UintBitfield <Offset, Length, T>
where Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
      Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero,
      <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>,
      T: PackedType + PartialEq,
{
    fn default() -> Self {
        Self::new(T::Type::zero()).expect(
            "Error getting the default. This is a bug.")
    }
}

#[cfg(test)]
mod bool_bitfield_tests {
    //! The boolean bitfield should contain everything needed to pack
    //! a boolean value into an integer with the default 32-bit output.
    use super::*;

    #[test]
    /// The BoolBitfield should pack a boolean into the correct location in 
    /// a u32.
    fn test_pack () {
 
         // We test a few cases, including the limits
         let bf = BoolBitfield::<U0>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<0);
 
         let bf = BoolBitfield::<U1>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<1);
 
         let bf = BoolBitfield::<U10>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<10);

         let bf = BoolBitfield::<U15>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<15);

         let bf = BoolBitfield::<U30>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<30);

         let bf = BoolBitfield::<U31>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<31);
 
         let bf = BoolBitfield::<U7>::new(false).unwrap();
         assert_eq!(bf.pack().unwrap(), 0);

         let bf = BoolBitfield::<U21>::new(false).unwrap();
         assert_eq!(bf.pack().unwrap(), 0);
    }

    /// The BoolBitfield should implement the Default trait, which should be
    /// false.
    #[test]
    fn test_default () {

        fn check_default(_bf: impl Default) {
        }

        let bf = BoolBitfield::<U0>::new(false).unwrap();
        check_default(bf);

        assert_eq!(bf, BoolBitfield::<U0>::default());
    }

    #[test]
    /// The BoolBitfield should implement the Into<bool> trait, supporting
    /// exporting its internal value.
    fn test_into () {
 
         // We test a few cases, including the limits
         let bf = BoolBitfield::<U0>::new(true).unwrap();
         // Until type ascription is stable, we need an intermediate variable
         // (type ascription would be `bf.into(): u32`)`
         let into_val: bool = bf.into();
         assert_eq!(into_val, true);
         let bf = BoolBitfield::<U10>::new(true).unwrap();
         let into_val: bool = bf.into();
         assert_eq!(into_val, true);

         let bf = BoolBitfield::<U7>::new(false).unwrap();
         let into_val: bool = bf.into();
         assert_eq!(into_val, false);

         let bf = BoolBitfield::<U21>::new(false).unwrap();
         let into_val: bool = bf.into();
         assert_eq!(into_val, false);
    }

    #[test]
    /// The BoolBitfield should impement the BitfieldLength trait
    fn test_bitfield_length () {

        // Length is always 1.
        assert_eq!(<BoolBitfield<U0> as BitfieldLength>::Length::USIZE, 1usize);
        assert_eq!(<BoolBitfield<U6> as BitfieldLength>::Length::USIZE, 1usize);
        assert_eq!(<BoolBitfield<U10> as BitfieldLength>::Length::USIZE, 1usize);

        assert_eq!(<BoolBitfield<U20> as BitfieldLength>::Length::U32, 1u32);
    }

    #[test]
    /// The BoolBitfield should implement into_inner, supporting
    /// exporting its internal value.
    fn test_into_inner () {
 
         // We test a few cases, including the limits
         let bf = BoolBitfield::<U0>::new(true).unwrap();
         // Until type ascription is stable, we need an intermediate variable
         // (type ascription would be `bf.into_inner(): u32`)`
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, true);
 
         let bf = BoolBitfield::<U10>::new(true).unwrap();
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, true);

         let bf = BoolBitfield::<U7>::new(false).unwrap();
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, false);

         let bf = BoolBitfield::<U21>::new(false).unwrap();
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, false);
    }

    #[test]
    /// The BoolBitfield should unpack bitfields to the correct bitfield object
    fn test_unpack() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U0>::unpack(1u32<<0).unwrap(), bf);

        let bf = BoolBitfield::<U1>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U1>::unpack(1u32<<1).unwrap(), bf);

        let bf = BoolBitfield::<U7>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U7>::unpack(1u32<<7).unwrap(), bf);

        let bf = BoolBitfield::<U25>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U25>::unpack(1u32<<25).unwrap(), bf);

        let bf = BoolBitfield::<U31>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U31>::unpack(1u32<<31).unwrap(), bf);

        // A couple of false tests
        let bf = BoolBitfield::<U3>::new(false).unwrap();
        assert_eq!(BoolBitfield::<U3>::unpack(0).unwrap(), bf);

        let bf = BoolBitfield::<U29>::new(false).unwrap();
        assert_eq!(BoolBitfield::<U29>::unpack(0).unwrap(), bf);
    }

    #[test]
    /// Unpacking a word should ignore bits outside of the bitfield
    fn test_unpack_with_extra_bits () {
        // Unpacks to true
        let val = (1u32 << 31) | (1u32 << 19) | (1u32 << 30);
        let bf = BoolBitfield::<U31>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U31>::unpack(val).unwrap(), bf);

        // And false
        let val = (1u32 << 31) | (1u32 << 19) | (1u32 << 30);
        let bf = BoolBitfield::<U12>::new(false).unwrap();
        assert_eq!(BoolBitfield::<U12>::unpack(val).unwrap(), bf);
    }
}

#[cfg(test)]
mod bool_bitfield_u16_tests {
    //! The boolean bitfield should contain everything needed to pack
    //! a boolean value into an explicitly set PackedU16 packed type.
    use super::*;
    use crate::PackedU16;

    #[test]
    /// The BoolBitfield should pack a boolean into the correct location in 
    /// a u16.
    fn test_pack () {
 
         // We test a few cases, including the limits
         let bf = BoolBitfield::<U0, PackedU16>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<0);
 
         let bf = BoolBitfield::<U1, PackedU16>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<1);
 
         let bf = BoolBitfield::<U10, PackedU16>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<10);

         let bf = BoolBitfield::<U15, PackedU16>::new(true).unwrap();
         assert_eq!(bf.pack().unwrap(), 1<<15);

         let bf = BoolBitfield::<U7, PackedU16>::new(false).unwrap();
         assert_eq!(bf.pack().unwrap(), 0);

         let bf = BoolBitfield::<U13, PackedU16>::new(false).unwrap();
         assert_eq!(bf.pack().unwrap(), 0);
    }

    #[test]
    /// The BoolBitfield should implement into_inner, supporting
    /// exporting its internal value.
    fn test_into_inner () {
 
         // We test a few cases, including the limits
         let bf = BoolBitfield::<U0, PackedU16>::new(true).unwrap();
         // Until type ascription is stable, we need an intermediate variable
         // (type ascription would be `bf.into_inner(): u32`)`
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, true);
 
         let bf = BoolBitfield::<U10, PackedU16>::new(true).unwrap();
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, true);

         let bf = BoolBitfield::<U7, PackedU16>::new(false).unwrap();
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, false);

         let bf = BoolBitfield::<U15, PackedU16>::new(false).unwrap();
         let into_val: bool = bf.into_inner();
         assert_eq!(into_val, false);
    }

    #[test]
    /// The BoolBitfield should unpack bitfields to the correct bitfield object
    fn test_unpack() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0, PackedU16>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U0, PackedU16>::unpack(1u16<<0).unwrap(), bf);

        let bf = BoolBitfield::<U1, PackedU16>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U1, PackedU16>::unpack(1u16<<1).unwrap(), bf);

        let bf = BoolBitfield::<U7, PackedU16>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U7, PackedU16>::unpack(1u16<<7).unwrap(), bf);

        let bf = BoolBitfield::<U15, PackedU16>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U15, PackedU16>::unpack(1u16<<15).unwrap(), bf);

        // A couple of false tests
        let bf = BoolBitfield::<U3, PackedU16>::new(false).unwrap();
        assert_eq!(BoolBitfield::<U3, PackedU16>::unpack(0).unwrap(), bf);

        let bf = BoolBitfield::<U12, PackedU16>::new(false).unwrap();
        assert_eq!(BoolBitfield::<U12, PackedU16>::unpack(0).unwrap(), bf);
    }

    #[test]
    /// Unpacking a word should ignore bits outside of the bitfield
    fn test_unpack_with_extra_bits () {
        // Unpacks to true
        let val = (1u16 << 15) | (1u16 << 12) | (1u16 << 8);
        let bf = BoolBitfield::<U15, PackedU16>::new(true).unwrap();
        assert_eq!(BoolBitfield::<U15, PackedU16>::unpack(val).unwrap(), bf);

        // And false
        let val = (1u16 << 15) | (1u16 << 12) | (1u16 << 8);
        let bf = BoolBitfield::<U4, PackedU16>::new(false).unwrap();
        assert_eq!(BoolBitfield::<U4, PackedU16>::unpack(val).unwrap(), bf);
    }
}

#[cfg(test)]
mod uint_bitfield_tests {
    //! The UintBitfield should contain everything needed to pack
    //! a uint value into an integer.
    
    use super::*;
    use rand::{thread_rng, Rng};

    #[test]
    /// The UintBitfield should pack a value into the correct location in 
    /// a u32.
    fn test_pack () {
        let mut rng = thread_rng();
        const ALL_ONES: u32 = !0;

        // Start with a few corner cases
        // Full width, all ones
        let val: u32 = ALL_ONES;
        let bf = UintBitfield::<U0, U32>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val);

        // Hard against upper bound
        let val: u32 = ALL_ONES >> 4;
        let bf = UintBitfield::<U4, U28>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val << 4);

        // No offset
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U0, U27>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val);

        // Check a few random values
        let val: u32 = rng.gen_range(0, 2u32.pow(6));
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val << 10);

        let val: u32 = rng.gen_range(0, 2u32.pow(15));
        let bf = UintBitfield::<U17, U15>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val << 17);

        let val: u32 = rng.gen_range(0, 2u32.pow(10));
        let bf = UintBitfield::<U0, U10>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val);
    }
    
    /// The UintBitfield should implement the Default trait, which should be
    /// 0.
    #[test]
    fn test_default () {

        fn check_default(_bf: impl Default) {
        }

        let bf = UintBitfield::<U0, U10>::new(0).unwrap();
        check_default(bf);

        assert_eq!(bf, UintBitfield::<U0, U10>::default());
    }

    #[test]
    /// The UintBitfield should impement the BitfieldLength trait
    fn test_bitfield_length () {

        // Length is set with the Length generic parameter
        assert_eq!(<UintBitfield<U0, U10> as BitfieldLength>::Length::USIZE, 10usize);
        assert_eq!(<UintBitfield<U5, U5> as BitfieldLength>::Length::USIZE, 5usize);
        assert_eq!(<UintBitfield<U12, U8> as BitfieldLength>::Length::U16, 8u16);

    }


    #[test]
    /// The UintBitfield should implement the Into<u32> trait, supporting
    /// exporting its internal value.
    fn test_into_inner () {
        let mut rng = thread_rng();

        // Check a few random values
        let val: u32 = rng.gen_range(0, 2u32.pow(6));
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        // Until type ascription is stable, we need an intermediate variable
        // (type ascription would be `bf.into_inner(): u32`)`
        let into_val: u32 = bf.into_inner();
        assert_eq!(into_val, val);

        let val: u32 = rng.gen_range(0, 2u32.pow(15));
        let bf = UintBitfield::<U17, U15>::new(val).unwrap();
        let into_val: u32 = bf.into_inner();
        assert_eq!(into_val, val);

        let val: u32 = rng.gen_range(0, 2u32.pow(10));
        let bf = UintBitfield::<U0, U10>::new(val).unwrap();
        let into_val: u32 = bf.into_inner();
        assert_eq!(into_val, val);
    }

    #[test]
    /// Invalid values should result in a BitfieldError::OverRange
    fn test_invalid_val () {
        const ALL_ONES: u32 = !0;

        let val: u32 = ALL_ONES;
        let res = UintBitfield::<U0, U31>::new(val);

        if let Err(error) = res {
            if let BitfieldError::OverRange { description: _ } = error {
                assert!(true);
            } else {
                panic!("The error should be a BitfieldError::OverRange");
            }
        } else {
            panic!("The bitfield should error");
        }

        let val: u32 = 0b11;
        let res = UintBitfield::<U0, U1>::new(val);
        if let Err(error) = res {
            if let BitfieldError::OverRange { description: _ } = error {
                assert!(true);
            } else {
                panic!("The error should be a BitfieldError::OverRange");
            }
        } else {
            panic!("The bitfield should error");
        }

        let val: u32 = ALL_ONES >> 15;
        let res = UintBitfield::<U10, U12>::new(val);

        if let Err(error) = res {
            if let BitfieldError::OverRange { description: _ } = error {
                assert!(true);
            } else {
                panic!("The error should be a BitfieldError::OverRange");
            }
        } else {
            panic!("The bitfield should error");
        }
    }
    
    #[test]
    /// The UintBitfield should unpack a word to the correct bitfield object.
    fn test_unpack () {
        let mut rng = thread_rng();
        const ALL_ONES: u32 = !0;

        // Start with a few corner cases
        // Full width, all ones
        let val: u32 = ALL_ONES;
        let bf = UintBitfield::<U0, U32>::new(val).unwrap();
        assert_eq!(UintBitfield::<U0, U32>::unpack(val).unwrap(), bf);

        // Hard against upper bound        
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U5, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U5, U27>::unpack(val << 5).unwrap(), bf);

        // No offset
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U0, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U0, U27>::unpack(val).unwrap(), bf);

        // Check a couple of random values
        let val: u32 = rng.gen_range(0, 2u32.pow(6));
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        assert_eq!(UintBitfield::<U10, U6>::unpack(val << 10).unwrap(), bf);

        let val: u32 = rng.gen_range(0, 2u32.pow(12));
        let bf = UintBitfield::<U15, U12>::new(val).unwrap();
        assert_eq!(UintBitfield::<U15, U12>::unpack(val << 15).unwrap(), bf);

    }

    #[test]
    /// The UintBitfield should ignore bits that are not in the bitfield
    /// when unpacking
    fn test_unpack_with_extra_bits () {
        let mut rng = thread_rng();
        const ALL_ONES: u32 = !0;

        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U5, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U5, U27>::unpack(ALL_ONES).unwrap(), bf);

        // No offset
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U0, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U0, U27>::unpack(ALL_ONES).unwrap(), bf);

        // Check a couple of random values
        let full_range_rand: u32 = rng.gen();
        let mask = ALL_ONES >> (32 - 6);
        let val: u32 = (full_range_rand >> 10) & mask;
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        assert_eq!(UintBitfield::<U10, U6>::unpack(val << 10).unwrap(), bf);

        let full_range_rand: u32 = rng.gen();
        let mask = ALL_ONES >> (32 - 12);
        let val: u32 = (full_range_rand >> 15) & mask;
        let bf = UintBitfield::<U15, U12>::new(val).unwrap();
        assert_eq!(UintBitfield::<U15, U12>::unpack(val << 15).unwrap(), bf);

    }

    #[test]
    ///
    fn test_custom_packed_type () {
        let packed_data = 0xFFFFFFFF;

        #[derive(Debug, PartialEq)]
        struct Packed24;
        impl PackedType for Packed24 {
            type Type = u32;
            type PackedLength = U24;
        }

        let bf1 = UintBitfield::<U8, U8, Packed24>::unpack(packed_data).unwrap();
        assert_eq!(bf1.val, 0xFF);

        let bf2 = UintBitfield::<U8, U8, Packed24>::new(0xFF).unwrap();
        assert_eq!(bf2.pack().unwrap(), 0xFF << 8);

        assert_eq!(bf1, bf2);
    }

}
