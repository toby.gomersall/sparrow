//! Sparrow-bitpacker is a crate for representing bitfields and packing them
//! into or unpacking from primitive types, making extensive use of compile 
//! time constraints on what is possible.
//!
//! The original use case is in defining bitfields within register maps, such
//! that it is impossible for the user to mistakenly set the wrong bits if they
//! are setting the right, correctly-defined bitfield object, with bitfield
//! consistency and correctness enforced at compile time (such as the bitfield
//! must fit within the containing type, and the specified packed length).
//!
//! It should be useful with any situation in which a bitfield map is known 
//! ahead of time and is static.
//!
//! For example, if we want to define a UintBitfield, offset by 6 bits and of
//! length 8 bits, all to fit within a 32-bit u32, we can do so:
//! ```
//! use sparrow_bitpacker::{UintBitfield, Bitfield};
//! use typenum::consts::*;
//! let val: u32 = 13;
//! type Offset = U6; // Typenum constants U6 := 6
//! type Length = U8; // := 8
//! // Note that the packed type defaults to 32-bits wide.
//! let my_bitfield = UintBitfield::<Offset, Length>::new(val).unwrap();
//!
//! let packed_result = my_bitfield.pack().unwrap();
//!
//! assert_eq!(packed_result, val << 6);
//! ```
//! We can use different packing types, the main obvious ones being included,
//! with an example for packing into a `u16` as:
//! ```
//! use sparrow_bitpacker::{UintBitfield, Bitfield, PackedU16};
//! use typenum::consts::*;
//! let val: u16 = 13;
//! type Offset = U6; // Typenum constants U6 := 6
//! type Length = U8; // := 8
//! let my_bitfield = UintBitfield::<Offset, Length, PackedU16>::new(val).unwrap();
//!
//! let packed_result = my_bitfield.pack().unwrap();
//!
//! assert_eq!(packed_result, val << 6);
//! ```
//! Or entirely custom packing types, in which the available bits and the
//! output type are not the same size, which is useful if the resultant packed
//! output is not the same size as a primitive type:
//! ```
//! use sparrow_bitpacker::{UintBitfield, Bitfield, PackedType};
//! use typenum::consts::*;
//!
//! // We also need PartialEq for the Bitfield trait
//! #[derive(PartialEq)]
//! struct PackedU24 {}
//!
//! impl PackedType for PackedU24 {
//!     type Type = u32;
//!     type PackedLength = U24;
//! }
//!
//! let val: u32 = 13;
//! type Offset = U6;
//! type Length = U8;
//! let my_bitfield = UintBitfield::<Offset, Length, PackedU24>::new(val).unwrap();
//!
//! let packed_result = my_bitfield.pack().unwrap();
//!
//! assert_eq!(packed_result, val << 6);
//! ```
//! This approach is similarly useful if you want to pack bitfields inside
//! another bitfield:
//! ```
//! use sparrow_bitpacker::{UintBitfield, BoolBitfield, Bitfield, PackedType};
//! use typenum::consts::*;
//!
//! type ControlWordLength = U8;
//!
//! #[derive(PartialEq)]
//! struct PackedControlWord {}
//!
//! impl PackedType for PackedControlWord {
//!     type Type = u32;
//!     type PackedLength = ControlWordLength;
//! }
//!
//! // Define a couple of bits in the control field at bits 2 and 4:
//! let go_field = BoolBitfield::<U2, PackedControlWord>::new(true).unwrap();
//! let stop_field = BoolBitfield::<U4, PackedControlWord>::new(false).unwrap();
//!
//! let packed_control = go_field.pack().unwrap() | stop_field.pack().unwrap();
//! let control_field = UintBitfield::<U12, ControlWordLength>::new(packed_control).unwrap();
//!
//! let packed_register = control_field.pack().unwrap();
//!
//! assert_eq!(packed_register, (1 << 2 | 0 << 4) << 12);
//! ```
//! If you try to set a bitfield with an invalid set of offset, length and
//! packed type, the compile will catch it:
//! ```compile_fail
//! use sparrow_bitpacker::{UintBitfield, Bitfield, PackedU16};
//! use typenum::consts::*;
//! type Offset = U12; 
//! type Length = U5; // Too big to fit inside a U16
//!
//! let my_bitfield = UintBitfield::<Offset, Length, PackedU16>::new(0);
//! ```

pub mod bitfields;
pub mod packed_types;
pub use bitfields::{
    Bitfield,
    BitfieldLength,
    BoolBitfield,
    UintBitfield,
    BitfieldResult,
    BitfieldError};

pub use packed_types::{
    TypeLength,
    PackedType,
    PackedU8,
    PackedU16,
    PackedU32,
    PackedU64,
    PackedU128};
