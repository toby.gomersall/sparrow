use std::fs::File;
use std::io::{Read, Error as IoError};
use std;
use strict_yaml_rust::{
    StrictYamlLoader,
    scanner::ScanError};
use snafu::{Snafu, ResultExt};

pub mod bitfields;
pub mod multibitfield;
pub mod register;
pub mod registers_layout;

use registers_layout::RegistersLayout;

#[derive(Debug, Snafu)]
/// Errors that are thrown when the YAML document cannot be loaded properly.
pub enum YamlFileLoadError {
    #[snafu(display("Cannot open the YAML file: {}", source))]
    OpeningFileError { source: IoError },
    #[snafu(display("The file requested contains invalid data: {}", source))]
    InvalidFileData { source: IoError },
    #[snafu(display("Error scanning the YAML document: {}", source))]
    YAMLScanError { source: ScanError },
    #[snafu(display("Error parsing the YAML to our schema: {}", source))]
    YAMLParseError { source: registers_layout::LayoutParseError },
}

pub type YamlFileLoadResult<T, E = YamlFileLoadError> = std::result::Result<T, E>;

/// Loads a register layout from a YAML file.
pub fn registers_layout_from_file(yaml_filename: &str) -> YamlFileLoadResult<RegistersLayout> {

    let mut yaml_file = File::open(yaml_filename).context(OpeningFileError)?;
    let mut yaml_string = String::new();

    yaml_file.read_to_string(&mut yaml_string).context(InvalidFileData)?;

    let yaml = StrictYamlLoader::load_from_str(&yaml_string).context(YAMLScanError)?;

    RegistersLayout::from_strictyaml(&yaml[0]).context(YAMLParseError)
}

