use indexmap::IndexMap;
use syn;
use strict_yaml_rust::StrictYaml;
use snafu::{Snafu, ResultExt};

use crate::bitfields::{
    BitfieldParseError,
    BitfieldTypes};

#[derive(Debug, PartialEq, Snafu)]
pub enum MultiBitfieldParseError {
    #[snafu(display("Cannot parse the multi-bitfield for field \"{}\": {}", name, source))]
    MultiBitfieldError { name: String, source: BitfieldParseError },
    #[snafu(display("No bitfields yaml keys"))]
    MissingBitfieldsYaml,
    #[snafu(display("Invalid bitfield name: \"{}\"", name))]
    InvalidBitfieldName { name: String },
}

pub type MultiBitfieldParseResult<T, E = MultiBitfieldParseError> = std::result::Result<T, E>;

#[derive(Debug, PartialEq)]
pub struct MultiBitfieldDef {
    pub fields: IndexMap<String, BitfieldTypes>,
}

/// Represents a collection of Bitfields
impl MultiBitfieldDef {

    pub(crate) fn from_strictyaml(bitfield_yaml: &StrictYaml) -> MultiBitfieldParseResult<Self> {

        match bitfield_yaml {
            StrictYaml::Hash(bitfield_config) => {

                let mut bitfields = MultiBitfieldDef {fields: IndexMap::new()};

                for (bitfield_name, bitfield_config_yaml) in bitfield_config {
                    let bitfield_name_str =
                        bitfield_name.as_str().unwrap().to_string();

                    if syn::parse_str::<syn::Ident>(&bitfield_name_str).is_err() {
                        // We have to explicitly return here so our if statement
                        // has type ()

                        return Err(MultiBitfieldParseError::InvalidBitfieldName {
                            name: bitfield_name_str });
                    }

                    let parsed_bitfield = BitfieldTypes::from_strictyaml(
                        bitfield_config_yaml)
                        .context(MultiBitfieldError { name: bitfield_name_str.clone() })?;

                    bitfields.fields.insert(bitfield_name_str, parsed_bitfield);
                }

                Ok(bitfields)

            },
            _ => Err(MultiBitfieldParseError::MissingBitfieldsYaml),
        }

    }

}

