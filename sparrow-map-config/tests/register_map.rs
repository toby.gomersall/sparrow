use indexmap::IndexMap;

use sparrow_map_config::registers_layout_from_file;
use sparrow_map_config::{
    bitfields::{
        BitfieldTypes,
        BoolBitfieldDef,
        UintBitfieldDef,
        UintSha1HashBitfieldDef},
    multibitfield::MultiBitfieldDef,
    register::{Register, RegisterType},
    registers_layout::RegistersLayout};

#[test]
fn test_valid_map() {

    let layout = registers_layout_from_file("tests/test_registers.yaml").unwrap();

    let mut expected_layout = RegistersLayout { layout: IndexMap::new() };

    let mut expected_control_mbf = MultiBitfieldDef { fields: IndexMap::new() };
    expected_control_mbf.fields.insert(
        "go".to_string(), BitfieldTypes::Bool(
            BoolBitfieldDef {
                description: Some("Makes it go".to_string()),
                offset: 0,
                default: Some(true),
            }
        ));
    expected_control_mbf.fields.insert(
        "stop".to_string(), BitfieldTypes::Bool(
            BoolBitfieldDef {
                description: Some("Makes it stop".to_string()),
                offset: 1,
                default: None
            }
        ));
    expected_control_mbf.fields.insert(
        "length".to_string(), BitfieldTypes::Uint(
            UintBitfieldDef {
                description: None,
                offset: 2,
                length: 10,
                default: Some(10)
            }
        ));

    expected_layout.layout.insert(
        "control".to_string(), Register {
            description: Some("A write-only control register".to_string()),
            index: 0,
            reg_type: RegisterType::WriteOnly,
            bitfields: expected_control_mbf
        });

    let mut expected_status_mbf = MultiBitfieldDef { fields: IndexMap::new() };
    expected_status_mbf.fields.insert(
        "whatson".to_string(), BitfieldTypes::Uint(
            UintBitfieldDef {
                description: None,
                offset: 0,
                length: 20,
                default: None
            }
        ));
    expected_status_mbf.fields.insert(
        "isitgood".to_string(), BitfieldTypes::Bool(
            BoolBitfieldDef {
                description: None,
                offset: 1,
                default: None
            }
        ));

    expected_layout.layout.insert(
        "status".to_string(), Register {
            description: None,
            index: 1,
            reg_type: RegisterType::ReadOnly,
            bitfields: expected_status_mbf
        });

    let mut expected_config_mbf = MultiBitfieldDef { fields: IndexMap::new() };
    expected_config_mbf.fields.insert(
        "size".to_string(), BitfieldTypes::Uint(
            UintBitfieldDef {
                description: Some("This is the size of the object".to_string()),
                offset: 0,
                length: 10,
                default: None
            }
        ));
    expected_config_mbf.fields.insert(
        "power".to_string(), BitfieldTypes::Uint(
            UintBitfieldDef {
                description: None,
                offset: 1,
                length: 5,
                default: None
            }
        ));
    expected_config_mbf.fields.insert(
        "frobinate".to_string(), BitfieldTypes::Bool(
            BoolBitfieldDef {
                description: Some("do a frobination when go happens".to_string()),
                offset: 2,
                default: Some(false)
            }
        ));

    expected_layout.layout.insert(
        "config".to_string(), Register {
            index: 2,
            description: Some(
                "Some sort of configuration for setting the \
                internals of the system.".to_string()),
            reg_type: RegisterType::ReadWrite,
            bitfields: expected_config_mbf
        });

    let mut expected_hash_mbf = MultiBitfieldDef { fields: IndexMap::new() };
    expected_hash_mbf.fields.insert(
        "word".to_string(), BitfieldTypes::UintSha1Hash(
            UintSha1HashBitfieldDef {
                uint_bitfield: UintBitfieldDef {
                    description: Some("The word containing the first 8 \
                        digits of the digest".to_string()),
                    offset: 0,
                    length: 32,
                    default: None
                },
                digest_digits: 8,
                digest_offset: 0,
            }
        ));

    expected_layout.layout.insert(
        "hex_digest".to_string(), Register {
            index: 3,
            description: Some(
                "The sha-1 hash digest of this YAML file (first 8 digits)".to_string()),
            reg_type: RegisterType::ReadOnly,
            bitfields: expected_hash_mbf
        });
    assert_eq!(layout, expected_layout);
}

