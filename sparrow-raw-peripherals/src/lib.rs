
pub mod traits;
pub mod errors;
pub mod mmap;
pub mod mock;
pub mod peripheral_word;

pub use traits::RawPeripheral;
pub use errors::{
    PeripheralError,
    PeripheralResult};
pub use peripheral_word::RawPeripheralWord;

