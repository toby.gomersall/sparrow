//! Defines a mock raw peripheral which on writes calls the requisite
//!

use std::cell::RefCell;
use std::io;

use super::{
    RawPeripheral,
    RawPeripheralWord,
    PeripheralResult};
use crate::errors::{ReadError, WriteError};

use std::collections::HashMap;
use std::rc::Rc;
use std::marker::PhantomData;
use num::NumCast;
use typenum;

use snafu::ResultExt;
use sparrow_bitpacker::{PackedType, PackedU32};

/// A handler to pass to a MockPeripheral which will handle the read and
/// write calls as they are made.
pub trait MockHandler<T = PackedU32> 
where T: PackedType,
{
    /// By default, we do not allow a read, this is because we have no sensible
    /// information to pass back to the calling code.
    fn handle_read(&mut self, index: usize) -> io::Result<T::Type> {
        Err(io::Error::new(
                io::ErrorKind::NotConnected, 
                format!(
                    "Read not implemented on MockHandler for index {}", index)))
    }

    /// `handle_write` will succeed by default, but the default case is to do
    /// nothing and quietly drop the write.
    #[allow(unused_variables)]
    fn handle_write(&mut self, index: usize, value: T::Type) -> io::Result<()> {
        Ok(())
    }

}

/// A `MockPeripheral` will delegate read and write calls from the 
/// super function to the handler that is passed in. The handler should
/// implement the `MockHandler` trait.
/// ```
/// use sparrow_raw_peripherals::{RawPeripheral, PeripheralResult};
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MockHandler};
/// use sparrow_bitpacker::PackedU16; 
/// use std::collections::HashMap;
/// use std::io;
/// use rand;
///
/// // A trivial example that simply caches the write values
/// struct MyHandler {
///     data: HashMap<usize, u16>,
/// }
///
/// impl MyHandler {
///     fn new() -> Self {
///         MyHandler { data: HashMap::new() }
///     }
///
/// }
///
/// impl MockHandler<PackedU16> for MyHandler {
///     fn handle_read(&mut self, index: usize) -> io::Result<u16> {
///         let opt_val = self.data.get(&index);
///         let ret_val = match opt_val {
///             Some(val) => val,
///             None => &0u16,
///         };
///         Ok(*ret_val)
///     }
///
///     fn handle_write(&mut self, index: usize, val: u16) -> io::Result<()> {
///         self.data.insert(index, val);
///         Ok(())
///     }
/// }
///
/// let mut handler = MyHandler::new();
/// let p = MockPeripheral::new(&mut handler);
/// let read_val = p.read(132).unwrap();
/// assert_eq!(read_val, 0);
/// 
/// let rand_val: u16 = rand::random();
/// p.write(132, rand_val);
///
/// let read_val = p.read(132).unwrap();
/// assert_eq!(read_val, rand_val);
/// ```
pub struct MockPeripheral <'a, H, T = PackedU32> 
where H: MockHandler<T>,
      T: PackedType,
{
    handler: Rc<RefCell<&'a mut H>>,
    t_marker: PhantomData<T>,
}

impl <'a, H, T> MockPeripheral <'a, H, T> 
where H: MockHandler<T>,
      T: PackedType,
{

    /// Creates a new mock peripheral with the provided callbacks.
    pub fn new(handler: &'a mut H) -> Self {

        MockPeripheral {
            handler: Rc::new(RefCell::new(handler)), t_marker: PhantomData }
    }
}

impl <'a, H, T> RawPeripheral<T> for MockPeripheral <'a, H, T>
where H: MockHandler<T>,
      T: PackedType,
{
    /// Register writes are passed straight through to the write handler
    /// ```
    /// use std::io;
    /// use sparrow_raw_peripherals::{RawPeripheral, PeripheralResult};
    /// use sparrow_raw_peripherals::mock::{MockPeripheral, MockHandler};
    ///
    /// struct MyWriteHandler {
    ///     write_idx: usize,
    ///     write_val: u32,
    /// }
    /// 
    /// impl MockHandler for MyWriteHandler {
    ///     fn handle_write(&mut self, index: usize, val: u32) -> io::Result<()> {
    ///         self.write_idx = index;
    ///         self.write_val = val;
    ///
    ///         Ok(())
    ///     }
    /// }
    ///
    /// let mut handler = MyWriteHandler { write_idx: 0, write_val: 0};
    /// let p = MockPeripheral::new(&mut handler);
    /// p.write(10, 241);
    ///
    /// assert_eq!(handler.write_idx, 10);
    /// assert_eq!(handler.write_val, 241);
    /// ```
    fn write(&self, index: usize, value: T::Type) -> PeripheralResult<()> {

        let mut handler = self.handler.borrow_mut();
        handler.handle_write(index, value).context(WriteError)
    }

    /// Register reads are delegated to the read handler.
    /// ```
    /// use sparrow_raw_peripherals::{RawPeripheral, PeripheralResult};
    /// use sparrow_raw_peripherals::mock::{MockPeripheral, MockHandler};
    /// use rand;
    /// use std::io;
    ///
    /// struct MyReadHandler {
    ///     read_val: u32,
    ///     last_idx: usize,
    /// }
    /// 
    /// impl MockHandler for MyReadHandler {
    ///     fn handle_read(&mut self, index: usize) -> io::Result<u32> {
    ///         self.read_val = rand::random();
    ///         self.last_idx = index;
    ///         Ok(self.read_val)
    ///     }
    /// }
    ///
    /// let mut handler = MyReadHandler { read_val: 0, last_idx: 0 };
    /// let p = MockPeripheral::new(&mut handler);
    /// let read_val = p.read(15).unwrap();
    ///
    /// assert_eq!(handler.last_idx, 15);
    /// assert_eq!(handler.read_val, read_val);
    /// ```
    fn read(&self, index: usize) -> PeripheralResult<T::Type> {
        let mut handler = self.handler.borrow_mut();
        handler.handle_read(index).context(ReadError)
    }

    fn to_word<IDX: typenum::Unsigned>(&self) -> RawPeripheralWord<Self, IDX, T> {

        let rc_handler = Rc::clone(&self.handler);
        let rc_peripheral = MockPeripheral {
            handler: rc_handler, t_marker: PhantomData};

        RawPeripheralWord::<Self, IDX, T>::new(rc_peripheral)
    }
}

/// A simple handler that acts as a block of memory initialised to zero.
/// That is, writes will set values and reads will read those back.
/// Reads to locations that have not been written to will read as 0.
///
/// Read and writes should happen through the peripheral interface.
///
/// It is implemented internally as a hashmap.
/// ```
/// use sparrow_raw_peripherals::{RawPeripheral, PeripheralResult};
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
/// use sparrow_bitpacker::PackedU16; 
/// use std::collections::HashMap;
/// use rand;
///
/// let mut handler = MemoryHandler::<PackedU16>::new();
/// let p = MockPeripheral::new(&mut handler);
///
/// // A read before a write returns 0
/// let read_val = p.read(132).unwrap();
/// assert_eq!(read_val, 0);
///
/// // Write a random value to a location
/// let rand_val: u16 = rand::random();
/// p.write(132, rand_val);
///
/// // which is now updated
/// let read_val = p.read(132).unwrap();
/// assert_eq!(read_val, rand_val);
///
/// // write a different value to the location
/// let rand_val: u16 = rand::random();
/// p.write(132, rand_val);
///
/// // and it is updated again.
/// let read_val = p.read(132).unwrap();
/// assert_eq!(read_val, rand_val);
///
/// // Read a different location, which is still 0
/// let read_val = p.read(100).unwrap();
/// assert_eq!(read_val, 0);
/// ```
pub struct MemoryHandler <T>
where T: PackedType,
{
    data: HashMap<usize, T::Type>,
    t_marker: PhantomData<T>,
}

impl <T> MemoryHandler <T>
where T: PackedType
{
    pub fn new() -> Self {
        MemoryHandler { data: HashMap::new() , t_marker: PhantomData }
    }

}

impl <T> Default for MemoryHandler<T>
where T: PackedType
{
    fn default() -> MemoryHandler<T> {
        MemoryHandler::new()
    }
}

impl <T> MockHandler <T> for MemoryHandler <T>
where T: PackedType,
{
    fn handle_read(&mut self, index: usize) -> io::Result<T::Type> {
        let opt_val = self.data.get(&index);
        let zero_val: T::Type = NumCast::from(0).unwrap();

        let ret_val = match opt_val {
            Some(val) => val,
            None => &zero_val,
        };
        Ok(*ret_val)
    }

    fn handle_write(&mut self, index: usize, val: T::Type) -> io::Result<()> {
        self.data.insert(index, val);
        Ok(())
    }
}




