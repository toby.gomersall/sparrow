
pub mod registers {

    use sparrow_raw_peripherals::RawPeripheral;

    pub mod control {

        use sparrow_registers::WriteOnlyRegister;

        use typenum::consts::*;
        use sparrow_bitpacker::{
            BoolBitfield,
            UintBitfield,
            Bitfield,
            BitfieldResult,
            PackedType,
            PackedU32};

        /// control has index 0
        pub type Register<P> = WriteOnlyRegister<P, Bitfields, U0, PackedU32>;

        #[derive(Debug, PartialEq, Copy, Clone)]
        pub struct Bitfields {
            pub go: Option<BoolBitfield<U0, PackedU32>>,
            pub stop: Option<BoolBitfield<U1, PackedU32>>,
            pub length: Option<UintBitfield<U2, U10, PackedU32>>,
        }

        impl Bitfield for Bitfields {

            /// Packs up the bitfields into a u32 (inside a `Result`).
            /// 
            /// If a bitfield is not set, then its packed value is 0.
            /// ```
            /// use sparrow_registers::registers::registers;
            /// use sparrow_bitpacker::bitfields::Bitfield;
            /// use rand::{thread_rng, Rng};
            /// let mut rng = thread_rng();
            ///
            /// let bitfield = registers::control::Bitfields::new(
            ///     Some(true), None, None).unwrap();
            /// assert_eq!(bitfield.pack().unwrap(), 1 << 0);
            /// 
            /// let bitfield = registers::control::Bitfields::new(
            ///     None, Some(true), None).unwrap();
            /// assert_eq!(bitfield.pack().unwrap(), 1 << 1);
            /// 
            /// let val: u32 = rng.gen_range(0, 2u32.pow(10));
            /// let bitfield = registers::control::Bitfields::new(
            ///     None, None, Some(val)).unwrap();
            /// assert_eq!(bitfield.pack().unwrap(), val << 2);
            /// ```
            fn pack(&self) -> BitfieldResult<<PackedU32 as PackedType>::Type> {

                // FIXME should use a default in preference to simply zero.
                // Probably best done as a constant on this module (though
                // no currently easy way to do compile time constant of
                // non-trivial types - `const fn` can't handle an unwrap
                // at the moment.
                // Ideally, we'd like to use:
                // https://github.com/rust-lang/rust/issues/51999
                let mut packed_result = 0;

                packed_result |= match &self.go {
                    Some(go) => go.pack()?,
                    None => 0
                };

                packed_result |= match &self.stop {
                    Some(stop) => stop.pack()?,
                    None => 0
                };

                packed_result |= match &self.length {
                    Some(length) => length.pack()?,
                    None => 0
                };

                Ok(packed_result)
            }

            /// Unpacks bitfields from a `u32` and returns a `Bitfields` object
            /// wrapped up in a BitfieldResult.
            ///
            /// If any of the fields cannot be constructed properly from data
            /// in the `u32`, an error is propagated.
            /// ```
            /// use sparrow_registers::registers::registers;
            /// use sparrow_bitpacker::bitfields::Bitfield;
            /// use rand::{thread_rng, Rng};
            /// let mut rng = thread_rng();
            ///
            /// let go: bool = rng.gen();
            /// let stop: bool = rng.gen();
            /// let length: PackedType::T = rng.gen_range(0, 2u32.pow(10));
            /// let bitfield = registers::control::Bitfields::new(
            ///     Some(go), Some(stop), Some(length)).unwrap();
            ///
            /// let packed_val = bitfield.pack().unwrap();
            ///
            /// assert_eq!(registers::control::Bitfields::unpack(packed_val).unwrap(), 
            ///            bitfield);
            /// ```
            fn unpack(raw_val: <PackedU32 as PackedType>::Type) -> BitfieldResult<Bitfields> {
                Ok(Bitfields {
                    go: Some(BoolBitfield::<U0, PackedU32>::unpack(raw_val)?),
                    stop: Some(BoolBitfield::<U1, PackedU32>::unpack(raw_val)?),
                    length: Some(UintBitfield::<U2, U10, PackedU32>::unpack(raw_val)?),
                })
            }
        }

        impl Bitfields {

            /// Creates a new control bitfield from the provided optional
            /// arguments.
            ///
            /// This bitfield is suitable for passing to control::Register
            ///
            pub fn new(go: Option<bool>, stop: Option<bool>, 
                       length: Option<<PackedU32 as PackedType>::Type>) -> BitfieldResult<Self> {

                let go = match go {
                    Some(val) => Some(BoolBitfield::<U0, PackedU32>::new(val)?),
                    None => None
                };

                let stop = match stop {
                    Some(val) => Some(BoolBitfield::<U1, PackedU32>::new(val)?),
                    None => None
                };

                let length = match length {
                    Some(val) => Some(UintBitfield::<U2, U10, PackedU32>::new(val)?),
                    None => None
                };

                Ok(Bitfields{ go, stop, length })
            }

            /// Updates the bitfield with values extracted from other.
            ///
            /// Only values in `other` that are not `None` will cause
            /// an update.
            /// ```
            /// use sparrow_registers::registers::registers::control::Bitfields;
            ///
            /// let mut bf = Bitfields::new(Some(true), None, Some(15)).unwrap();
            /// let updating_bf = Bitfields::new(None, Some(true), Some(512)).unwrap();
            ///
            /// bf.update(&updating_bf);
            ///
            /// assert_eq!(bf, Bitfields::new(Some(true), Some(true), Some(512)).unwrap());
            /// ```
            pub fn update(self: &mut Self, other: &Self) {

                if other.go.is_some() {
                    self.go = other.go;
                }

                if other.stop.is_some() {
                    self.stop = other.stop;
                }

                if other.length.is_some() {
                    self.length = other.length;
                }
            }
        }

        pub struct ExtractedBitfields {
            pub go: bool,
            pub stop: bool,
            pub length: <PackedU32 as PackedType>::Type,
        }

        impl From<Bitfields> for ExtractedBitfields {

            /// Creates an [`ExtractedBitfields`] from a [`Bitfields`], in 
            /// which the types are as expected for downstream usage.
            ///
            /// Current policy is for a `None` in `Bitfields` to map to its
            /// equivalent "zero" value (i.e. 0 or false).
            /// ```
            /// use sparrow_registers::registers::registers::control;
            /// let bf = control::Bitfields::new(Some(true), None, Some(15)).unwrap();
            ///
            /// let ex_bf = control::ExtractedBitfields::from(bf);
            ///
            /// assert_eq!(ex_bf.go, true);
            /// assert_eq!(ex_bf.stop, false);
            /// assert_eq!(ex_bf.length, 15);
            /// ```
            /// [`Into`] is also automatically inferred.
            /// ```
            /// # use sparrow_registers::registers::registers::control;
            /// 
            /// let bf = control::Bitfields::new(None, Some(false), Some(12)).unwrap();
            /// let ex_bf: control::ExtractedBitfields = bf.into();
            /// 
            /// assert_eq!(ex_bf.go, false);
            /// assert_eq!(ex_bf.stop, false);
            /// assert_eq!(ex_bf.length, 12);
            /// ```
            fn from(control_bitfield: Bitfields) -> Self {

                // FIXME Is this the correct behaviour? Should be merrily
                // put zeros in place of a None? See `pack()`.
                // Perhaps we want to error on None? this would be ok.
                // It would be preferable to have some kind of default
                // value, but I can see that this always returning a valid
                // ExtractedBitfields object could be desirable...
                let go = match control_bitfield.go {
                    Some(val) => val.into_inner(),
                    None => false
                };

                let stop = match control_bitfield.stop {
                    Some(val) => val.into_inner(),
                    None => false
                };

                let length = match control_bitfield.length {
                    Some(val) => val.into_inner(),
                    None => 0
                };

                ExtractedBitfields { go, stop, length, }
            }
        }
        
        /// Returns a bitfield loaded with only the `go` field set to `val`.
        /// ```
        /// use sparrow_registers::registers::registers::control;
        ///
        /// assert_eq!(control::go(true), control::Bitfields::new(Some(true), None, None));
        /// assert_eq!(control::go(false), control::Bitfields::new(Some(false), None, None));
        /// ```
        /// This can be combined with other fields using the [`update`] method.
        pub fn go(val: bool) -> BitfieldResult<Bitfields> {
            Bitfields::new(Some(val), None, None)
        }

        /// Returns a bitfield loaded with only the `stop` field set to `val`.
        /// ```
        /// use sparrow_registers::registers::registers::control;
        ///
        /// assert_eq!(control::stop(true), control::Bitfields::new(None, Some(true), None));
        /// assert_eq!(control::stop(false), control::Bitfields::new(None, Some(false), None));
        /// ```
        /// This can be combined with other fields using the [`update`] method.
        pub fn stop(val: bool) -> BitfieldResult<Bitfields> {
            Bitfields::new(None, Some(val), None)
        }

        /// Returns a bitfield loaded with only the `stop` field set to `val`.
        /// ```
        /// use sparrow_registers::registers::registers::control;
        /// use sparrow_bitpacker::BitfieldError;
        ///
        /// assert_eq!(control::length(12), control::Bitfields::new(None, None, Some(12)));
        /// // Errors should be propagated as expected...
        /// const MAX_VAL: u32 = !0u32 >> (32 - 10);
        /// let test_val = MAX_VAL + 10;
        /// assert_eq!(control::length(test_val).err(),
        ///            Some(BitfieldError::OverRange));
        /// ```
        /// This can be combined with other fields using the [`update`] method.
        pub fn length(val: <PackedU32 as PackedType>::Type) -> BitfieldResult<Bitfields> {
            Bitfields::new(None, None, Some(val))
        }
    }

    /// A register layout encapsulates instances of registers and manages
    /// the underlying raw peripheral.
    /// ```
    /// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
    /// use sparrow_registers::registers::registers;
    ///
    /// let mut handler = MemoryHandler::new();
    /// let p = MockPeripheral::new(&mut handler);

    /// let reg_layout = registers::RegistersLayout::new(p);
    ///
    /// let mut bitfield = registers::control::fields::go(true).unwrap();
    /// bitfield.update(registers::control::fields::length(10).unwrap());
    ///
    /// reg_layout.config.write(bitfield).unwrap();
    ///
    /// assert_eq!(p.read().unwrap(), bitfield.pack().unwrap());
    /// ```
    pub struct RegistersLayout<P>
    where P: RawPeripheral,
    {
        #[allow(dead_code)]
        raw_peripheral: P,
        pub control: control::Register<P>,
    }
    impl<P> RegistersLayout<P>
    where P: RawPeripheral,
    {
        pub fn new(raw_peripheral: P) -> Self {
            let control = control::Register::new(&raw_peripheral);
            RegistersLayout::<P> {
                raw_peripheral,
                control,
            }
        }
    }
}

fn main() {
    use sparrow_raw_peripherals::mock::{
        MockPeripheral, MemoryHandler, MockHandler};
    use sparrow_bitpacker::bitfields::Bitfield;
    use sparrow_registers::registers::WriteableRegister;

    let mut handler = MemoryHandler::new();
    let p = MockPeripheral::new(&mut handler);

    let mut reg_layout = registers::RegistersLayout::new(p);

    let mut bitfield = registers::control::go(true).unwrap();
    bitfield.update(&registers::control::length(10).unwrap());

    reg_layout.control.write(bitfield).unwrap();

    let stored_val = handler.handle_read(0).unwrap();

    assert_eq!(stored_val, bitfield.pack().unwrap());
}

#[cfg(test)]
mod register_map_tests {

    use super::*;

    #[test]
    fn do_test () {
        use sparrow_raw_peripherals::mock::{
            MockPeripheral, MemoryHandler, MockHandler};
        use sparrow_bitpacker::bitfields::Bitfield;
        use sparrow_registers::registers::WriteableRegister;

        let mut handler = MemoryHandler::new();
        let p = MockPeripheral::new(&mut handler);

        let reg_layout = registers::RegistersLayout::new(p);

        let mut bitfield = registers::control::go(true).unwrap();
        bitfield.update(&registers::control::length(10).unwrap());

        reg_layout.control.write(bitfield).unwrap();

        let stored_val = handler.handle_read(0).unwrap();

        assert_eq!(stored_val, bitfield.pack().unwrap());
    }
}
