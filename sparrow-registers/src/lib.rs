
pub mod registers;

pub use registers::{
    RegisterError,
    RegisterResult,
    WriteableRegister,
    ReadableRegister,
    ReadWriteRegister,
    WriteOnlyRegister,
    ReadOnlyRegister};
