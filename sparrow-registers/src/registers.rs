use sparrow_bitpacker::{Bitfield, BitfieldError, PackedType, PackedU32};
use sparrow_raw_peripherals::{
    RawPeripheral, RawPeripheralWord, PeripheralResult, PeripheralError};
use snafu::{Snafu, ResultExt};
use typenum::Unsigned;
use std::marker::PhantomData;

#[derive(Debug, Snafu)]
pub enum RegisterError {
    #[snafu(display("Error packing the bitfield: {}", "source"))]
    BitfieldPackingError {source: BitfieldError},
    #[snafu(display("Error unpacking the bitfield: {}", "source"))]
    BitfieldUnpackingError {source: BitfieldError},
    #[snafu(display("Error writing the register: {}", "source"))]
    WriteError {source: PeripheralError},
    #[snafu(display("Error reading the register: {}", "source"))]
    ReadError {source: PeripheralError},
}

pub type RegisterResult<T, E = RegisterError> = std::result::Result<T, E>;

/// Defines a register that is writeable.
pub trait WriteableRegister<BF, T = PackedU32>
where BF: Bitfield<T>,
      T: PackedType + PartialEq,
{

    /// Implements the low level write to the RawPeripheral
    fn write_raw(&mut self, packed_val: T::Type) -> PeripheralResult<()>;

    /// Packs up the bitfields into a T::Type and writes the result out.
    ///
    /// The packing is delegated to the Bitfield and the write itself
    /// to [`write_raw`](WriteableRegister::write_raw).
    fn write(&mut self, bitfields: BF) -> RegisterResult<()> {
        let packed_val =
            bitfields.pack().context(BitfieldPackingError)?;

        self.write_raw(packed_val).context(WriteError)
    }
}

/// Defines a register that is readable.
pub trait ReadableRegister<BF, T = PackedU32>
where BF: Bitfield<T>,
      T: PackedType + PartialEq,
{

    /// Implements the low level read from the RawPeripheral
    fn read_raw(&self) -> PeripheralResult<T::Type>;

    /// Reads the raw value from the peripheral with
    /// [`read_raw`](ReadableRegister::read_raw) and then packs up the result into
    /// the associated Bitfield.
    fn read(&self) -> RegisterResult<BF> {
        let raw_val = self.read_raw().context(ReadError)?;
        BF::unpack(raw_val).context(BitfieldUnpackingError)
    }
}

/// A Write-only register that implements only the
/// [`WriteableRegister`] trait.
///
/// The index of the register is set at compile time using
/// [`typenum`].
///
/// It wraps a raw peripheral, to which the writes are delegated.
///
/// ```
/// use sparrow_registers::{WriteableRegister, WriteOnlyRegister};
/// use sparrow_bitpacker::BoolBitfield;
/// use sparrow_raw_peripherals::RawPeripheral;
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
/// use typenum::consts::*;
///
/// let mut handler = MemoryHandler::new();
/// let p = MockPeripheral::new(&mut handler);
///
/// type Register<P> = WriteOnlyRegister<P, BoolBitfield<U15>, U10>;
///
/// let mut register = Register::new(&p);
///
/// // We set bit 15 of the register with a BoolBitfield instance.
/// register.write(BoolBitfield::<U15>::new(true).unwrap());
///
/// let written_word = p.read(10).unwrap();
///
/// assert_eq!(written_word, 1<<15);
/// ```
///
pub struct WriteOnlyRegister <P, BF, IDX, T = PackedU32>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq
{
    peripheral_word: RawPeripheralWord<P, IDX, T>,
    addr_marker: PhantomData<IDX>,
    bf_marker: PhantomData<BF>,
    t_marker: PhantomData<T>,
}

impl <P, BF, IDX, T> WriteOnlyRegister <P, BF, IDX, T>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{
    /// Creates an instance of `WriteOnlyRegister` that wraps `raw_peripheral`.
    pub fn new(raw_peripheral: &P) -> Self {
        let peripheral_word = RawPeripheralWord::<_, IDX, T>::from(raw_peripheral);

        Self {
            peripheral_word,
            addr_marker: PhantomData,
            bf_marker: PhantomData,
            t_marker: PhantomData}
    }
}

impl <P, BF, IDX, T> WriteableRegister<BF, T> for WriteOnlyRegister <P, BF, IDX, T>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{

    fn write_raw(&mut self, packed_val: T::Type) -> PeripheralResult<()> {
        self.peripheral_word.write(packed_val)
    }

}

/// A Read-only register that implements only the 
/// [`ReadableRegister`] trait.
/// 
/// The index of the register is set at compile time using 
/// [`typenum`].
///
/// It wraps a raw peripheral, to which the reads are delegated.
///
/// ```
/// use sparrow_registers::{ReadableRegister, ReadOnlyRegister};
/// use sparrow_bitpacker::UintBitfield;
/// use sparrow_raw_peripherals::RawPeripheral;
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
/// use typenum::consts::*;
/// use rand;
///
/// let mut handler = MemoryHandler::new();
/// let p = MockPeripheral::new(&mut handler);
///
/// type Register<P> = ReadOnlyRegister<P, UintBitfield<U15, U7>, U12>;
///
/// let register = Register::new(&p);
///
/// // Set our peripheral to have some data in (we set register 12
/// // as defined above). We use a complete 32-bit random value, which 
/// // allows the bitfield unpacking to do some useful work.
/// let full_word: u32 = rand::random();
/// p.write(12, full_word);
///
/// // Create a mask to select 7 bits with an offset of 15, as specified by
/// // the UintBitfield defined above.
/// let mask: u32 = (!0u32 >> (32 - 7)) << 15;
/// 
/// let read_val: u32 = register.read().unwrap().into_inner();
/// let expected_read_val = (full_word & mask) >> 15;
///
/// assert_eq!(read_val, expected_read_val);
/// ```
///
pub struct ReadOnlyRegister <P, BF, IDX, T = PackedU32>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{
    peripheral_word: RawPeripheralWord<P, IDX, T>,
    addr_marker: PhantomData<IDX>,
    bf_marker: PhantomData<BF>,
    t_marker: PhantomData<T>,
}

impl <P, BF, IDX, T> ReadOnlyRegister <P, BF, IDX, T>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{
    /// Creates an instance of `ReadOnlyRegister` that wraps `raw_peripheral`.
    pub fn new(raw_peripheral: &P) -> Self {
        let peripheral_word = RawPeripheralWord::<_, IDX, T>::from(raw_peripheral);

        Self {
            peripheral_word,
            addr_marker: PhantomData,
            bf_marker: PhantomData,
            t_marker: PhantomData}
    }
}

impl <P, BF, IDX, T> ReadableRegister<BF, T> for ReadOnlyRegister <P, BF, IDX, T>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{

    fn read_raw(&self) -> PeripheralResult<T::Type> {
        self.peripheral_word.read()
    }

}

/// A read and write register that implements both the 
/// [`ReadableRegister`](trait.ReadableRegister.html) and 
/// [`WriteableRegister`](trait.WriteableRegister.html) traits.
/// 
/// The index of the register is set at compile time using 
/// [`typenum`][typenum].
///
/// It wraps a raw peripheral, to which the reads and writes are delegated.
///
/// ```
/// use sparrow_registers::{ReadableRegister, WriteableRegister, ReadWriteRegister};
/// use sparrow_bitpacker::{UintBitfield, PackedU16};
/// use sparrow_raw_peripherals::RawPeripheral;
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
/// use typenum::consts::*;
/// use rand;
///
/// let mut handler = MemoryHandler::<PackedU16>::new();
/// let p = MockPeripheral::new(&mut handler);
/// 
/// // We show it here working with a 16-bit packed Uint.
/// type Register<P> = ReadWriteRegister<P, UintBitfield<U4, U10, PackedU16>, U12, PackedU16>;
///
/// let mut register = Register::new(&p);
/// 
/// let val: u16 = 12;
/// register.write(UintBitfield::<U4, U10, PackedU16>::new(val).unwrap());
///
/// let read_val: u16 = register.read().unwrap().into_inner();
///
/// assert_eq!(val, read_val);
/// ```
///
pub struct ReadWriteRegister <P, BF, IDX, T = PackedU32>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{
    peripheral_word: RawPeripheralWord<P, IDX, T>,
    addr_marker: PhantomData<IDX>,
    bf_marker: PhantomData<BF>,
    t_marker: PhantomData<T>,
}

impl <P, BF, IDX, T> ReadWriteRegister <P, BF, IDX, T>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{
    /// Creates an instance of `ReadWriteRegister` that wraps `raw_peripheral`.
    pub fn new(raw_peripheral: &P) -> Self {
        let peripheral_word = RawPeripheralWord::<_, IDX, T>::from(raw_peripheral);

        Self {
            peripheral_word,
            addr_marker: PhantomData,
            bf_marker: PhantomData,
            t_marker: PhantomData}
    }
}

impl <P, BF, IDX, T> ReadableRegister<BF, T> for ReadWriteRegister <P, BF, IDX, T>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{

    fn read_raw(&self) -> PeripheralResult<T::Type> {
        self.peripheral_word.read()
    }

}

impl <P, BF, IDX, T> WriteableRegister<BF, T> for ReadWriteRegister <P, BF, IDX, T>
where P: RawPeripheral<T>,
      BF: Bitfield<T>,
      IDX: Unsigned,
      T: PackedType + PartialEq,
{

    fn write_raw(&mut self, packed_val: T::Type) -> PeripheralResult<()> {
        self.peripheral_word.write(packed_val)
    }

}

